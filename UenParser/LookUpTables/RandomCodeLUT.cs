﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UenParser.LookUpTables
{
    public static class RandomCodeLUT
    {
        public static string GetMonthCode(string value)
        {
            var lut = new Dictionary<string, string>();
            lut["01"] = "A";    lut["02"] = "B";    lut["03"] = "C";
            lut["04"] = "D";    lut["05"] = "E";    lut["06"] = "F";
            lut["07"] = "G";    lut["08"] = "H";    lut["09"] = "I";
            lut["10"] = "J";    lut["11"] = "K";    lut["12"] = "L";
            return lut[value];
        }
        public static string GetDayCode(string value)
        {
            var lut = new Dictionary<string, string>();
            lut["01"] = "A"; lut["02"] = "B"; lut["03"] = "C";
            lut["04"] = "D"; lut["05"] = "E"; lut["06"] = "F";
            lut["07"] = "G"; lut["08"] = "H"; lut["09"] = "I";
            lut["10"] = "J"; lut["11"] = "K"; lut["12"] = "L";
            lut["13"] = "M"; lut["14"] = "N"; lut["15"] = "O";
            lut["16"] = "P"; lut["17"] = "Q"; lut["18"] = "R";
            lut["19"] = "S"; lut["20"] = "T"; lut["21"] = "U";
            lut["22"] = "V"; lut["23"] = "W"; lut["24"] = "X";
            lut["25"] = "Y"; lut["26"] = "Z"; lut["27"] = "1";
            lut["28"] = "2"; lut["29"] = "3"; lut["30"] = "4";
            lut["31"] = "5";
            return lut[value];
        }
    }
}
