﻿using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using log4net;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UenParser.Generators
{
    public static class QRCodeGenerator
    {
        private static ILog log = LogManager.GetLogger(typeof(EmergingPriceGenerator));

        public static Boolean ProduceOutput(string url)
        {
            //輸入要製作二維條碼的字串
            string codeString = url;

            //實例化，設定錯誤修正容量
            /*
              Level L (Low)      7%  of codewords can be restored. 
              Level M (Medium)   15% of codewords can be restored. 
              Level Q (Quartile) 25% of codewords can be restored. 
              Level H (High)     30% of codewords can be restored.
            */
            try
            {
                QrEncoder encoder = new QrEncoder(ErrorCorrectionLevel.H);

                //編碼
                QrCode code = encoder.Encode(codeString);

                //定義線條寬度
                int moduleSizeInPixels = 25;

                //繪二維條碼圖初始化
                GraphicsRenderer renderer = new GraphicsRenderer(new FixedModuleSize(moduleSizeInPixels, QuietZoneModules.Two), Brushes.Black, Brushes.White);

                //留白區大小
                Point padding = new Point(10, 16);

                //取得條碼圖大小
                DrawingSize dSize = renderer.SizeCalculator.GetSize(code.Matrix.Width);
                int imgWidth = dSize.CodeWidth + 2 * padding.X;
                int imgHeight = dSize.CodeWidth + 2 * padding.Y;
                //設定影像大小及解析度
                Bitmap img = new Bitmap(imgWidth, imgHeight);
                img.SetResolution(1200.0F, 1200.0F);

                //繪製二維條碼圖
                Graphics g = Graphics.FromImage(img);
                renderer.Draw(g, code.Matrix, padding);
                img.Save(AppDomain.CurrentDomain.BaseDirectory 
                            + @"\\Output\\QRCode.png", ImageFormat.Png);
                log.InfoFormat("繪製 QRCode 完成，Url位址為: {0}", url);
                return true;
            }
            catch (Exception ex)
            {
                log.ErrorFormat("錯誤: 繪製 QRCode 時遇到問題. 訊息: {0}", ex.Message);
                return false;
            }
        }
    }
}
