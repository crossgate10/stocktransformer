﻿using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Xsl;

namespace UenParser.Generators
{
    public static class IndexHTMLGenerator
    {
        private static ILog log = LogManager.GetLogger(typeof(OTCPriceGenerator));
        private const string exchangeHeaderFileName = "Exchange_header";
        private const string otcHeaderFileName = "OTC_header.js";

        public static void ProduceOutput(string sourcePath, string destPath)
        {
            using (StreamReader sr = File.OpenText(sourcePath + exchangeHeaderFileName + ".js"))
            {
                string s = sr.ReadToEnd();
                XmlDocument doc = JsonConvert.DeserializeXmlNode(s);
                //XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
                //XmlElement root = doc.DocumentElement;
                //doc.InsertBefore(xmlDeclaration, root);
                doc.Save(sourcePath + exchangeHeaderFileName + ".xml");

                //Create a new XslCompiledTransform and load the compiled transformation.
                XslCompiledTransform xslt = new XslCompiledTransform();
                xslt.Load(typeof(Transform));

                // Execute the transformation and output the results to a file.
                xslt.Transform(sourcePath + exchangeHeaderFileName + ".xml", sourcePath + exchangeHeaderFileName + ".html");
            }
        }
    }
}
