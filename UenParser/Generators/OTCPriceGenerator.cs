﻿using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using UenParser.JsonModel;

namespace UenParser.Generators
{
    public class OTCPriceGenerator : PriceGenerator
    {
        private static ILog log = LogManager.GetLogger(typeof(OTCPriceGenerator));

        List<List<object>> result = new List<List<object>>();
        string[] fileStream_header = null, fileStream_content = null, fileStream_summary = null;
        string fileStream_lastdayContent = null;
        OTCContent[] lastdayOTC;
        int errorNo = 0;

        string pattern_header = "(.{2})(.{18})(.{6})([\\+\\-])(.{4})(.{8})(.{12})";
        List<int> capturedIndexes_header = new List<int>() { 1, 3, 4, 5, 7 };

        string pattern_content = "(\\d{4}..)(.{2,6})(\\s{10})(.{6})(.{6})(.{6})(.{6})" +
                "([\\+\\-\\^\\svX])(.{6})(.{12})(.{6})(.{6})(.{24})(.{9})(.{22})(.{9})(.{19})(.{6})(.{2})";
        List<int> capturedIndexes_content = new List<int>() { 1, 2, 4, 5, 6, 7, 8, 9, 11, 12, 14, 16, 18, 19 };

        public override void Read(List<string> paths)
        {
            ReadLastdayData();
            ReadTodayData(paths);
        }

        private void ReadTodayData(List<string> paths)
        {
            Encoding enc = Encoding.GetEncoding(950);   // Big-5
            List<string> tmpStringArray;

            int pathFlag = 0;
            try
            {
                foreach (var path in paths)
                {
                    log.InfoFormat("取得今日的上櫃Source檔，檔案路徑: {0}", path);
                    tmpStringArray = new List<string>();
                    using (StreamReader sr = new StreamReader(path, enc))
                    {
                        while (sr.Peek() >= 0)
                        {
                            tmpStringArray.Add(sr.ReadLine());
                        }
                        switch (pathFlag)
                        {
                            case 0:
                                fileStream_header = tmpStringArray.ToArray();
                                break;
                            case 1:
                                fileStream_content = tmpStringArray.ToArray();
                                break;
                            case 2:
                                fileStream_summary = tmpStringArray.ToArray();
                                break;
                        }
                    }
                    pathFlag++;
                }
            }
            catch (Exception ex)
            {
                log.ErrorFormat("錯誤: 讀取上櫃資料來源檔案時遇到問題. 訊息: {0}", ex.Message);
            }
        }

        private void ReadLastdayData()
        {
            string homeDirPath = AppDomain.CurrentDomain.BaseDirectory;
            string backupDirPath = homeDirPath + "OutputBackup\\";
            List<string> dirs = new List<string>(Directory.EnumerateDirectories(backupDirPath));
            int dirAmount = dirs.Count;
            string lastdayDataPath = "";
            try
            {
                lastdayDataPath = dirs[dirAmount - 2];
            }
            catch (Exception)
            {
                lastdayDataPath = dirs[0];
            }
            log.InfoFormat("取得最近一次的Output備份檔，備份檔路徑: {0}", lastdayDataPath);
            List<string> files = new List<string>(Directory.EnumerateFiles(lastdayDataPath));

            foreach (var file in files)
            {
                if (file.Contains("OTC1"))
                {
                    string tmpString = "";
                    log.DebugFormat("讀取檔案: {0} 內容", file);
                    StreamReader sr = new StreamReader(file);
                    while (sr.Peek() >= 0)
                    {
                        tmpString = sr.ReadLine().Trim();
                    }
                    fileStream_lastdayContent = tmpString;
                    log.Debug("檔案讀取完成");
                }
            }
            int streamLength = fileStream_lastdayContent.Length;

            //取出JSON片段
            fileStream_lastdayContent = fileStream_lastdayContent.Substring(11, streamLength - 12);
            JavaScriptSerializer js = new JavaScriptSerializer();
            log.Debug("將讀取到的JSON字串轉為OTCContentModel[]");
            lastdayOTC = js.Deserialize<OTCContent[]>(fileStream_lastdayContent);
        }
        public override string[] Process(string pattern, List<int> capturedIndexes, string[] dataStream)
        {
            log.DebugFormat("開始parsing上櫃檔案，pattern: {0}", pattern);
            string usePattern = "";
            switch (pattern)
            {
                case "header":
                    usePattern = pattern_header;
                    break;
                case "content":
                    usePattern = pattern_content;
                    break;
            }
            Regex rx = new Regex(usePattern, RegexOptions.Compiled);

            string tmp = "";
            List<string> tmpResult = new List<string>();
            try
            {
                int line = 1;
                foreach (string str in dataStream)
                {
                    Match match = rx.Match(str);
                    tmp = "";
                    if (match.Success)
                    {
                        foreach (String s in capturedIndexes.Select<int, String>(i => (match.Groups[i].Value)))
                        {
                            tmp += s.Trim() + ",";
                        }
                        tmpResult.Add(tmp);
                        tmp = "";
                    }
                    else
                    {
                        log.WarnFormat("上櫃 {0} 檔第 {1} 行，有不符合pattern的格式", pattern, line);
                    }
                    line++;
                }
            }
            catch (Exception ex)
            {
                log.ErrorFormat("錯誤: 處理上櫃資料來源檔案時遇到問題. 訊息: {0}", ex.Message);
            }
            return tmpResult.ToArray();
        }

        public override List<List<object>> GenerateJson(List<string> paths)
        {
            Read(paths);
            string[] dataSet_header = Process("header", capturedIndexes_header, fileStream_header);
            log.DebugFormat("parsing上櫃header檔完成，共 {0} 筆", dataSet_header.Count());
            string[] dataSet_content = Process("content", capturedIndexes_content, fileStream_content);
            log.DebugFormat("parsing上櫃content檔完成，共 {0} 筆", dataSet_content.Count());

            result.Add(PutHeaderDataToModel(dataSet_header));
            result.Add(PutContentDataToModel(dataSet_content));
            return result;
        }

        //------class private function------
        private List<object> PutHeaderDataToModel(string[] header)
        {
            List<object> tmpResult = new List<object>();
            string[] dataArr;
            OTCHeder obj;

            log.Debug("逐一將parsing結果放到對應的OTCHeaderModel Attribute");
            foreach (var data in header)
            {
                dataArr = data.Split(',');
                obj = new OTCHeder();
                obj.Industry = dataArr[0];
                obj.Close = (Convert.ToDouble(dataArr[1]) / 100).ToString("F2");
                obj.SymbolClose = dataArr[2];
                obj.Change = (Convert.ToDouble(dataArr[3]) / 100).ToString("F2");
                obj.Volume = (Math.Round((Convert.ToDouble(dataArr[4]) / 1000), 0, MidpointRounding.AwayFromZero)).ToString();
                tmpResult.Add(obj);
            }
            return tmpResult;
        }

        private List<object> PutContentDataToModel(string[] content)
        {
            List<object> tmpResult = new List<object>();
            string[] dataArr;
            OTCContent obj;

            log.Debug("逐一將parsing結果放到對應的OTCContentModel Attribute");
            foreach (var data in content.Skip(1))
            {
                dataArr = data.Split(',');
                if (dataArr[0].Length > 4 && dataArr[0] != "006201")
                {
                    break;
                }

                string lastClose = "", lastRiseLimit = "", lastFallLimit = "";
                try
                {
                    var lastStock = (from n in lastdayOTC
                                     where n.Code == dataArr[0]
                                     select n).First();
                    lastClose = lastStock.Close;
                    //取出前一交易日的"次日漲跌停"
                    lastRiseLimit = lastStock.NextRiseLimit;
                    lastFallLimit = lastStock.NextFallLimit;
                }
                catch (Exception)
                {
                    log.WarnFormat("在最近一次的上櫃Output備份檔中找不到此股票: {0}", dataArr[1]);
                }

                obj = new OTCContent();
                obj.Code = dataArr[0];
                obj.Company = dataArr[1];
                try
                {
                    obj.Open = (Convert.ToDouble(dataArr[2]) / 100).ToString("F2"); //取小數後兩位
                }
                catch (Exception)
                {
                    obj.Open = dataArr[2];
                }
                try
                {
                    obj.DayHigh = (Convert.ToDouble(dataArr[3]) / 100).ToString("F2");
                }
                catch (Exception)
                {
                    obj.DayHigh = dataArr[3];
                }
                try
                {
                    obj.DayLow = (Convert.ToDouble(dataArr[4]) / 100).ToString("F2");
                }
                catch (Exception)
                {
                    obj.DayLow = dataArr[4];
                }
                try
                {
                    obj.Close = (Convert.ToDouble(dataArr[5]) / 100).ToString("F2");
                }
                catch (Exception)
                {
                    obj.Close = dataArr[5];
                }
                obj.SymbolClose = dataArr[6];
                try
                {
                    obj.Change = (Convert.ToDouble(dataArr[7]) / 100).ToString("F2");
                }
                catch (Exception)
                {
                    obj.Change = dataArr[7];
                }
                try
                {
                    obj.NextRiseLimit = (Convert.ToDouble(dataArr[8]) / 100).ToString("F2");
                }
                catch (Exception)
                {
                    obj.NextRiseLimit = dataArr[8];
                }
                try
                {
                    obj.NextFallLimit = (Convert.ToDouble(dataArr[9]) / 100).ToString("F2");
                }
                catch (Exception)
                {
                    obj.NextFallLimit = dataArr[9];
                }
                try
                {
                    obj.Volume = (Convert.ToInt32(dataArr[10])).ToString("N0"); //千位符號
                }
                catch (Exception)
                {
                    obj.Volume = (Convert.ToInt32(dataArr[10])).ToString();
                }
                try
                {
                    obj.Capital = (Convert.ToInt32(dataArr[11])).ToString("N0");
                }
                catch (Exception)
                {
                    obj.Capital = (Convert.ToInt32(dataArr[11])).ToString();
                }
                try
                {
                    obj.PE = (Convert.ToDouble(dataArr[12]) / 100).ToString("F2");
                }
                catch (Exception)
                {
                    obj.PE = dataArr[12];
                }
                obj.Industry = dataArr[13];
                obj.pq_cellcls = CheckSymbol(obj.SymbolClose, obj.Open, obj.DayHigh, obj.DayLow, lastClose, lastRiseLimit, lastFallLimit);
                tmpResult.Add(obj);
            }
            return tmpResult;
        }

        private object CheckSymbol(
            string symbolClose, string open, string dayHigh, string dayLow,
            string lastClose, string lastRiseLimit, string lastFallLimit)
        {
            string _change = "", _close = "", _open = "", _dayHigh = "", _dayLow = "";
            double openPrice = 0, dayHightPrice = 0, dayLowPrice = 0,
                lastClosePrice = 0, lastRiseLimitPrice = 0, lastFallLimitPrice = 0;

            try
            {
                openPrice = Convert.ToDouble(open);
                dayHightPrice = Convert.ToDouble(dayHigh);
                dayLowPrice = Convert.ToDouble(dayLow);
                lastClosePrice = Convert.ToDouble(lastClose);
                lastRiseLimitPrice = Convert.ToDouble(lastRiseLimit);
                lastFallLimitPrice = Convert.ToDouble(lastFallLimit);
            }
            catch (Exception e)
            {
                log.WarnFormat("指數轉型態時發生錯誤，Error Type: {0}", e.GetType().ToString());
            }

            switch (symbolClose)
            {
                case "+":
                    _change = "redFont";
                    _close = "redFont";
                    break;
                case "-":
                    _change = "greenFont";
                    _close = "greenFont";
                    break;
                case "^":
                    _change = "redBg";
                    _close = "redBg";
                    break;
                case "v":
                    _change = "greenBg";
                    _close = "greenBg";
                    break;
            }
            //開盤顏色
            if (openPrice > lastClosePrice)
            {
                _open = "redFont";
                if (openPrice >= lastRiseLimitPrice)
                {
                    _open = "redBg";
                }
            }
            else if (openPrice < lastClosePrice)
            {
                _open = "greenFont";
                if (openPrice <= lastFallLimitPrice)
                {
                    _open = "greenBg";
                }
            }
            //當日最高顏色
            if (dayHightPrice > lastClosePrice)
            {
                _dayHigh = "redFont";
                if (dayHightPrice >= lastRiseLimitPrice)
                {
                    _dayHigh = "redBg";
                }
            }
            else if (dayHightPrice < lastClosePrice)
            {
                _dayHigh = "greenFont";
                if (dayHightPrice <= lastFallLimitPrice)
                {
                    _dayHigh = "greenBg";
                }
            }
            //當日最低顏色
            if (dayLowPrice > lastClosePrice)
            {
                _dayLow = "redFont";
                if (dayLowPrice >= lastRiseLimitPrice)
                {
                    _dayLow = "redBg";
                }
            }
            else if (dayLowPrice < lastClosePrice)
            {
                _dayLow = "greenFont";
                if (dayLowPrice <= lastFallLimitPrice)
                {
                    _dayLow = "greenBg";
                }
            }

            var tmpResult = new
            {
                Change = (_change == "") ? null : _change,
                Open = (_open == "") ? null : _open,
                Close = (_close == "") ? null : _close,
                DayHigh = (_dayHigh == "") ? null : _dayHigh,
                DayLow = (_dayLow == "") ? null : _dayLow
            };

            return tmpResult;
        }
    }
}
