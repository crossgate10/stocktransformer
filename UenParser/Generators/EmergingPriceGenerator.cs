﻿using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UenParser.JsonModel;

namespace UenParser.Generators
{
    public class EmergingPriceGenerator : PriceGenerator
    {
        private static ILog log = LogManager.GetLogger(typeof(EmergingPriceGenerator));

        List<List<object>> result = new List<List<object>>();
        string fileStream = "", extension = "";
        string[] fileArray = null;

        public override void Read(List<string> paths)
        {
            Encoding enc = Encoding.GetEncoding(950);   // Big-5
            FileInfo fileInfo = new FileInfo(paths[0]);
            extension = fileInfo.Extension;
            try
            {
                if (extension == ".xls")
                {
                    fileArray = ReadExcelFile(paths[0]);
                }
                else if (extension == ".csv")
                {
                    using (StreamReader sr = new StreamReader(paths[0], enc))
                    {
                        fileStream = sr.ReadToEnd();
                    }
                }
                else
                {
                    MessageBox.Show("錯誤: 興櫃資料來源非 .xls 或 .csv!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("錯誤: 讀取興櫃資料來源檔案時遇到問題. 訊息: " + ex.Message);
            }
        }

        public override string[] Process(string pattern, List<int> capturedIndexes, string[] dataStream)
        {
            string[] tmpResult = null;
            if (extension == ".xls")
            {
                tmpResult = fileArray;
                return tmpResult;
            }
            else if (extension == ".csv")
            {
                tmpResult = SplitCSV(fileStream).ToArray<string>();
                return tmpResult.Skip(3).Take(tmpResult.Length - 7).ToArray();
            }
            else
            {
                MessageBox.Show("錯誤: 興櫃資料來源非 .xls 或 .csv!");
                return null;
            }
        }

        public override List<List<object>> GenerateJson(List<string> paths)
        {
            List<object> tmpResult = new List<object>();
            string[] dataArr;
            Emerging obj;

            Read(paths);
            string[] dataSet = Process(null, null, null);
            foreach (var data in dataSet)
            {
                dataArr = data.Split(new string[] { "|,|" }, StringSplitOptions.None);
                obj = new Emerging();
                obj.Code = dataArr[0];
                obj.Company = dataArr[1];
                try
                {
                    obj.PreAveragePrice = (Convert.ToDouble(dataArr[2])).ToString("F2"); //小數後兩位
                }
                catch (Exception)
                {
                    obj.PreAveragePrice = dataArr[2];
                }
                try
                {
                    obj.BuyingPrice = (Convert.ToDouble(dataArr[3])).ToString("F2");
                }
                catch (Exception)
                {
                    obj.BuyingPrice = dataArr[3];
                }
                try
                {
                    obj.SellingPrice = (Convert.ToDouble(dataArr[5])).ToString("F2");
                }
                catch (Exception)
                {
                    obj.SellingPrice = dataArr[5];
                }
                try
                {
                    obj.DayHigh = (Convert.ToDouble(dataArr[7])).ToString("F2");
                }
                catch (Exception)
                {
                    obj.DayHigh = dataArr[7];
                }
                try
                {
                    obj.DayLow = (Convert.ToDouble(dataArr[8])).ToString("F2");
                }
                catch (Exception)
                {
                    obj.DayLow = dataArr[8];
                }
                try
                {
                    obj.Close = (Convert.ToDouble(dataArr[10])).ToString("F2");
                }
                catch (Exception)
                {
                    obj.Close = dataArr[10];
                }
                try
                {
                    obj.Volume = (Convert.ToInt32(dataArr[12])).ToString("N0"); //千位符號
                }
                catch (Exception)
                {
                    obj.Volume = dataArr[12];
                }
                tmpResult.Add(obj);
            }
            result.Add(tmpResult);
            return result;
        }

        //-----class private function------
        private string[] SplitCSV(string p)
        {
            var s = p.Replace("\"", "|");
            return s.Split(new string[] { "\r\n" }, StringSplitOptions.None);
        }

        private string[] ReadExcelFile(string path)
        {
            var xlsx = new LinqToExcel.ExcelQueryFactory(path);

            var query =
                from item in xlsx.Worksheet("Sheet1")
                select item;

            List<string> tmpResult = new List<string>();
            string tmp = "";
            foreach (var row in query)
            {
                tmp = "";
                foreach (var element in row)
                {
                    tmp += element + "|,|";
                }
                tmpResult.Add(tmp);
            }

            return tmpResult.ToArray();
        }
    }
}
