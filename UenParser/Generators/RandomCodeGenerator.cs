﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UenParser.LookUpTables;

namespace UenParser.Generators
{
    public static class RandomCodeGenerator
    {
        private static ILog log = LogManager.GetLogger(typeof(EmergingPriceGenerator));

        public static string ProduceOutput(){

            DateTime dateOfToday = DateTime.Now;
            try
            {
                var month = RandomCodeLUT.GetMonthCode(dateOfToday.ToString("MM"));
                var day = RandomCodeLUT.GetDayCode(dateOfToday.ToString("dd"));
                var randomOneBitCode = Guid.NewGuid().ToString()
                                        .Replace("-", "").Substring(0, 1).ToUpper();
                string result = dateOfToday.ToString("yy") + month + day + randomOneBitCode;
                log.InfoFormat("產生亂碼名稱完成，檔名為: {0}.png", result);
                return result;
            }
            catch (Exception ex)
            {
                log.ErrorFormat("錯誤: 產生亂碼名稱時遇到問題. 訊息: {0}", ex.Message);
                return "";
            }
        }
    }
}
