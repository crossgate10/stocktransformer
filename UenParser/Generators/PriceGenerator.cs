﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UenParser
{
    public abstract class PriceGenerator
    {
        public abstract void Read(List<string> paths);

        public abstract string[] Process(string pattern, List<int> capturedIndexes, string[] dataStream);

        public abstract List<List<object>> GenerateJson(List<string> paths);

        public void ProduceOutput(List<List<object>> jsons, string outputFileName)
        {
            string baseDirPath = AppDomain.CurrentDomain.BaseDirectory;
            JsonSerializerSettings setting = new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore
            };

            //不包含Header資訊
            if (jsons.Count == 1)
            {
                string[] js = new[] { "var " + outputFileName.ToLower() + "Data = " + JsonConvert.SerializeObject(jsons[0], setting) + ";" };
                System.IO.File.WriteAllLines(baseDirPath + @"\\Output\\" +
                                    outputFileName + "_content.js", js);
            }
            //包含Header資訊
            else if (jsons.Count == 2)
            {
                string jsHeader = JsonConvert.SerializeObject(new { data = jsons[0].ToArray() }, setting);
                System.IO.File.WriteAllText(baseDirPath + @"\\Output\\" +
                                    outputFileName + "_header.js", jsHeader);
                string[] jsContent = new[] { "var " + outputFileName.ToLower() + "Data = " + JsonConvert.SerializeObject(jsons[1], setting) + ";" };
                System.IO.File.WriteAllLines(baseDirPath + @"\\Output\\" +
                                    outputFileName + "_content.js", jsContent);
            }
        }
    }
}
