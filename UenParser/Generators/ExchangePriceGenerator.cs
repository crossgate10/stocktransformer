﻿using log4net;
using log4net.Config;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using UenParser.JsonModel;

namespace UenParser.Generators
{
    public class ExchangePriceGenerator : PriceGenerator
    {
        private static ILog log = LogManager.GetLogger(typeof(ExchangePriceGenerator));

        List<List<object>> result = new List<List<object>>();
        string[] fileStream_header = null;
        string[] fileStream_content = null;
        string fileStream_lastdayContent = null;
        ExchangeContent[] lastdayExchange;

        string pattern_header = "(((\\d+)(.)(\\d+)(.)(\\d+)(.))|" +
                "((\\S{2})(\\d+\\S{1})\\s(\\S{2})(\\d+\\S{1})\\s(\\S{1})(\\d+\\S{1})\\s(\\S{1})(\\d+\\S{1})\\s(\\S{1})(\\d+\\S{1})\\s(\\S{3})(\\d+\\S{1})\\s(\\S{3})(\\d+\\S{1}))|" +
                "(\\S{5}(\\d+\\.\\d+)\\((.)\\S(\\d+\\.\\d+)\\))|" + 
                "(\\S{5}(\\d+\\.\\d+\\S{2}))|" +
                "(\\S{6}(\\d+\\.\\d+)\\((.)\\S(\\d+\\.\\d+)\\))|" +
                "(\\S{7}(\\d+\\.\\d+)\\((.)\\S(\\d+\\.\\d+)\\))|" +
                "(\\S{8}(\\d+\\.\\d+)\\((.)\\S(\\d+\\.\\d+)\\))|" +
                "(\\S{8}(\\d+\\.\\d+))|" +
                "(\\D{6,9}\\s+(\\d+\\.\\d+)\\((.)\\s+(\\d+\\.\\d+)\\))|" +
                ")";
        List<int> capturedIndexes_header = new List<int>() 
            { 
                3, 5, 7, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 
                25, 26, 27, 29, 31, 32, 33, 35, 36, 37, 39, 40, 41, 43, 45, 46, 47
            };

        String pattern_content = "([\\s1])(\\d{4}..)(.{3,6})([X^\\s])" +
                "(([\\d\\s]{3}\\d\\.\\d{2})|(\\s{5}-\\s))([\\+\\-\\s])(.{7})(.{6})(.{6})([\\+\\-\\s])(.{7})([\\+\\-\\s])(.{7})([\\+\\-\\s])(.{7})(.{6})(.{7})(.{7})" +
                "(.{6})(.{6})(.{6})(.{6})(.{6})(.{6})(.{6})(.{7})" +
                "((.{6})|([\\d\\s]{6})|(\\s{4}-\\s))";
        List<int> capturedIndexes_content = new List<int>() { 1, 2, 3, 4, 5, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29 };

        public override void Read(List<string> paths)
        {
            ReadLastdayData();
            ReadTodayData(paths);
        }

        private void ReadTodayData(List<string> paths)
        {
            Encoding enc = Encoding.GetEncoding(950);   // Big-5
            List<string> tmpStringArray;

            int pathFlag = 0;
            try
            {
                foreach (var path in paths)
                {
                    log.InfoFormat("取得今日的上市Source檔，檔案路徑: {0}", path);
                    tmpStringArray = new List<string>();
                    using (StreamReader sr = new StreamReader(path, enc))
                    {
                        while (sr.Peek() >= 0)
                        {
                            tmpStringArray.Add(sr.ReadLine());
                        }
                        if (pathFlag == 0)
                        {
                            fileStream_header = tmpStringArray.ToArray();
                        }
                        else
                        {
                            fileStream_content = tmpStringArray.ToArray();
                        }
                    }
                    pathFlag++;
                }
            }
            catch (Exception ex)
            {
                log.ErrorFormat("錯誤: 讀取上市資料來源檔案時遇到問題. 訊息: {0}", ex.Message);
            }
        }

        private void ReadLastdayData()
        {
            string homeDirPath = AppDomain.CurrentDomain.BaseDirectory;
            string backupDirPath = homeDirPath + "OutputBackup\\";
            List<string> dirs = new List<string>(Directory.EnumerateDirectories(backupDirPath));
            int dirAmount = dirs.Count;
            string lastdayDataPath = "";
            try
            {
                lastdayDataPath = dirs[dirAmount - 2];
            }
            catch (Exception)
            {
                lastdayDataPath = dirs[0];
            }
            log.InfoFormat("取得最近一次的Output備份檔，備份檔路徑: {0}", lastdayDataPath);
            List<string> files = new List<string>(Directory.EnumerateFiles(lastdayDataPath));

            foreach (var file in files)
            {
                if (file.Contains("Exchange1"))
                {
                    string tmpString = "";
                    log.DebugFormat("讀取檔案: {0} 內容", file);
                    StreamReader sr = new StreamReader(file);
                    while (sr.Peek() >= 0)
                    {
                        tmpString = sr.ReadLine().Trim();
                    }
                    fileStream_lastdayContent = tmpString;
                    log.Debug("檔案讀取完成");
                }
            }
            int streamLength = fileStream_lastdayContent.Length;

            //取出JSON片段
            fileStream_lastdayContent = fileStream_lastdayContent.Substring(11, streamLength - 12);
            JavaScriptSerializer js = new JavaScriptSerializer();
            log.Debug("將讀取到的JSON字串轉為ExchangeContentModel[]");
            lastdayExchange = js.Deserialize<ExchangeContent[]>(fileStream_lastdayContent);
        }

        public override string[] Process(string pattern, List<int> capturedIndexes, string[] dataStream)
        {
            log.DebugFormat("開始parsing上市檔案，pattern: {0}", pattern);
            string usePattern = "";
            if (pattern == "header")
            {
                usePattern = pattern_header;
            }
            else if (pattern == "content")
            {
                usePattern = pattern_content;
            }
            Regex rx = new Regex(usePattern, RegexOptions.Compiled);

            string tmp = "";
            List<string> tmpResult = new List<string>();
            try
            {
                int line = 1;
                foreach (string str in dataStream)
                {
                    Match match = rx.Match(str);
                    tmp = "";
                    if (match.Success)
                    {
                        foreach (String s in capturedIndexes.Select<int, String>(i => (match.Groups[i].Value)))
                        {
                            tmp += s.Trim() + ",";
                        }
                        tmpResult.Add(tmp);
                        tmp = "";
                    }
                    else
                    {
                        log.WarnFormat("上市 {0} 檔第 {1} 行，有不符合pattern的格式", pattern, line);
                    }
                    line++;
                }
            }
            catch (Exception ex)
            {
                log.ErrorFormat("錯誤: 處理上市資料來源檔案時遇到問題. 訊息: {0}", ex.Message);
            }
            return tmpResult.ToArray();
        }

        public override List<List<object>> GenerateJson(List<string> paths)
        {
            Read(paths);
            string[] dataSet_header = Process("header", capturedIndexes_header, fileStream_header);
            log.DebugFormat("parsing上市header檔完成，共 {0} 筆", dataSet_header.Count());
            string[] dataSet_content = Process("content", capturedIndexes_content, fileStream_content);
            log.DebugFormat("parsing上市content檔完成，共 {0} 筆", dataSet_content.Count());

            result.Add(PutHeaderDataToModel(dataSet_header));
            result.Add(PutContentDataToModel(dataSet_content));
            return result;
        }

        //------class private function------
        private List<object> PutContentDataToModel(string[] content)
        {
            List<object> tmpResult = new List<object>();
            string[] dataArr;
            ExchangeContent obj;

            int industryCount = 0;
            log.Debug("逐一將parsing結果放到對應的ExchangeContentModel Attribute");
            foreach (var data in content)
            {
                dataArr = data.Split(',');
                string lastClose = "";
                try
                {
                    lastClose = (from n in lastdayExchange
                                 where n.Code == dataArr[1]
                                 select n.Close).First();
                }
                catch (Exception)
                {
                    log.WarnFormat("在最近一次的上市Output備份檔中找不到此股票: {0}", dataArr[1]);
                }

                obj = new ExchangeContent();
                if (dataArr[0] == "1") { industryCount++; }
                obj.Industry = "." + industryCount.ToString() + ".";
                obj.Code = dataArr[1];
                obj.Company = dataArr[2];
                obj.Change = dataArr[4];
                obj.Close = dataArr[6];
                obj.NextRiseLimit = dataArr[7];
                obj.NextFallLimit = dataArr[8];
                obj.Open = dataArr[10];
                obj.DayHigh = dataArr[12];
                obj.DayLow = dataArr[14];
                obj.VolumePerTrade = dataArr[15];
                try
                {
                    obj.Volume = Convert.ToInt32(dataArr[16]).ToString("N0"); //千位符號
                }
                catch (Exception)
                {
                    obj.Volume = dataArr[16];
                }
                try
                {
                    obj.PreVolume = Convert.ToInt32(dataArr[17]).ToString("N0");
                }
                catch (Exception)
                {
                    obj.PreVolume = dataArr[17];
                }
                obj.RsiPer5 = dataArr[18];
                try
                {
                    obj.VolumePer5 = Convert.ToInt32(dataArr[19]).ToString("N0");
                }
                catch (Exception)
                {
                    obj.VolumePer5 = dataArr[19];
                }
                obj.PricePer5 = dataArr[20];
                obj.PricePer20 = dataArr[21];
                obj.BiasPer10 = dataArr[22];
                obj.LastYearHigh = dataArr[23];
                obj.LastYearLow = dataArr[24];
                obj.PE = dataArr[25];
                try
                {
                    obj.Capital = Convert.ToInt32(dataArr[26]).ToString("N0");
                }
                catch (Exception)
                {
                    obj.Capital = dataArr[26];
                }
                obj.pq_cellcls = CheckSymbol(dataArr[3], dataArr[5], dataArr[9], dataArr[11], dataArr[13], dataArr[10], dataArr[12], dataArr[14], lastClose);
                tmpResult.Add(obj);
            }
            return tmpResult;
        }

        private List<object> PutHeaderDataToModel(string[] header)
        {
            List<object> tmpResult = new List<object>();
            List<string> clearedData = new List<string>();
            string[] dataArr = ReplaceSymbol(header);
            ExchangeHeader obj = new ExchangeHeader();

            //計算除權交易參考價數量，調整後續索引, 51為沒有除權交易參考資訊的基礎值
            int offset = header.Length - 51;
            log.Debug("逐一將parsing結果放到對應的ExchangeHeaderModel Attribute");
            obj.Date = dataArr[0].Split(',')[0] +
                (Int32.Parse(dataArr[0].Split(',')[1]) < 10 ? "0" + dataArr[0].Split(',')[1] : dataArr[0].Split(',')[1]) +
                (Int32.Parse(dataArr[0].Split(',')[2]) < 10 ? "0" + dataArr[0].Split(',')[2] : dataArr[0].Split(',')[2]);
            obj.RiseFallAmount = new
            {
                RiseLimit = dataArr[1].Split(',')[4],
                FallLimit = dataArr[1].Split(',')[6],
                Rise = dataArr[1].Split(',')[8],
                Fall = dataArr[1].Split(',')[10],
                Same = dataArr[1].Split(',')[12],
                NonDeal = dataArr[1].Split(',')[14],
                NonComparePrice = dataArr[1].Split(',')[16],
            };
            obj.ClosingIndex = new 
            {
                Value = dataArr[3].Split(',')[17],
                Symbol = dataArr[3].Split(',')[18],
                SymbolValue = dataArr[3].Split(',')[19]
            };
            obj.DealAmount = dataArr[4].Split(',')[20];
            obj.NonFinance = new
            {
                Value = dataArr[6].Split(',')[21],
                Symbol = dataArr[6].Split(',')[22],
                SymbolValue = dataArr[6].Split(',')[23]
            };
            obj.NonElectronics = new
            {
                Value = dataArr[7].Split(',')[21],
                Symbol = dataArr[7].Split(',')[22],
                SymbolValue = dataArr[7].Split(',')[23]
            };
            obj.NonFinanceElectronics = new
            {
                Value = dataArr[8].Split(',')[27],
                Symbol = dataArr[8].Split(',')[28],
                SymbolValue = dataArr[8].Split(',')[29]
            };
            obj.DayHighIndex = new
            {
                Value = dataArr[10].Split(',')[24],
                Symbol = dataArr[10].Split(',')[25],
                SymbolValue = dataArr[10].Split(',')[26]
            };
            obj.DayLowIndex = new
            {
                Value = dataArr[11].Split(',')[24],
                Symbol = dataArr[11].Split(',')[25],
                SymbolValue = dataArr[11].Split(',')[26]
            };
            obj.TW50Index = new
            {
                Value = dataArr[12].Split(',')[24],
                Symbol = dataArr[12].Split(',')[25],
                SymbolValue = dataArr[12].Split(',')[26]
            };
            obj.FormosaIndex = new
            {
                Value = dataArr[13].Split(',')[17],
                Symbol = dataArr[13].Split(',')[18],
                SymbolValue = dataArr[13].Split(',')[19]
            };
            obj.SalaryHigh100Index = new
            {
                Value = dataArr[14].Split(',')[27],
                Symbol = dataArr[14].Split(',')[28],
                SymbolValue = dataArr[14].Split(',')[29]
            };
            obj.ElectronicsRewardIndex = new
            {
                Value = dataArr[15].Split(',')[27],
                Symbol = dataArr[15].Split(',')[28],
                SymbolValue = dataArr[15].Split(',')[29]
            };
            obj.FinanceRewardIndex = new
            {
                Value = dataArr[16].Split(',')[27],
                Symbol = dataArr[16].Split(',')[28],
                SymbolValue = dataArr[16].Split(',')[29]
            };
            for (int i = 0; i < offset; i++)
            {
                obj.ExRightReferencePrice = dataArr[17 + i].Split(',');
            }
            obj.CementAndCeramic = new
            {
                Value = dataArr[18 + offset].Split(',')[31],
                Symbol = dataArr[18 + offset].Split(',')[32],
                SymbolValue = dataArr[18 + offset].Split(',')[33]
            };
            obj.PlasticAndChemical = new
            {
                Value = dataArr[19 + offset].Split(',')[31],
                Symbol = dataArr[19 + offset].Split(',')[32],
                SymbolValue = dataArr[19 + offset].Split(',')[33]
            };
            obj.Electrical = new
            {
                Value = dataArr[20 + offset].Split(',')[31],
                Symbol = dataArr[20 + offset].Split(',')[32],
                SymbolValue = dataArr[20 + offset].Split(',')[33]
            };
            obj.Cement = new
            {
                Value = dataArr[21 + offset].Split(',')[31],
                Symbol = dataArr[21 + offset].Split(',')[32],
                SymbolValue = dataArr[21 + offset].Split(',')[33]
            };
            obj.Food = new
            {
                Value = dataArr[22 + offset].Split(',')[31],
                Symbol = dataArr[22 + offset].Split(',')[32],
                SymbolValue = dataArr[22 + offset].Split(',')[33]
            };
            obj.Plastic = new
            {
                Value = dataArr[23 + offset].Split(',')[31],
                Symbol = dataArr[23 + offset].Split(',')[32],
                SymbolValue = dataArr[23 + offset].Split(',')[33]
            };
            obj.Textile = new
            {
                Value = dataArr[24 + offset].Split(',')[31],
                Symbol = dataArr[24 + offset].Split(',')[32],
                SymbolValue = dataArr[24 + offset].Split(',')[33]
            };
            obj.ElectricMachinery = new
            {
                Value = dataArr[25 + offset].Split(',')[31],
                Symbol = dataArr[25 + offset].Split(',')[32],
                SymbolValue = dataArr[25 + offset].Split(',')[33]
            };
            obj.ElectricalAndCable = new
            {
                Value = dataArr[26 + offset].Split(',')[31],
                Symbol = dataArr[26 + offset].Split(',')[32],
                SymbolValue = dataArr[26 + offset].Split(',')[33]
            };
            obj.ChemicalBiotechnologyAndMedicalCare = new
            {
                Value = dataArr[27 + offset].Split(',')[31],
                Symbol = dataArr[27 + offset].Split(',')[32],
                SymbolValue = dataArr[27 + offset].Split(',')[33]
            };
            obj.GlassAndCeramic = new
            {
                Value = dataArr[28 + offset].Split(',')[31],
                Symbol = dataArr[28 + offset].Split(',')[32],
                SymbolValue = dataArr[28 + offset].Split(',')[33]
            };
            obj.PaperAndPulp = new
            {
                Value = dataArr[29 + offset].Split(',')[31],
                Symbol = dataArr[29 + offset].Split(',')[32],
                SymbolValue = dataArr[29 + offset].Split(',')[33]
            };
            obj.IronAndSteel = new
            {
                Value = dataArr[30 + offset].Split(',')[31],
                Symbol = dataArr[30 + offset].Split(',')[32],
                SymbolValue = dataArr[30 + offset].Split(',')[33]
            };
            obj.Rubber = new
            {
                Value = dataArr[31 + offset].Split(',')[31],
                Symbol = dataArr[31 + offset].Split(',')[32],
                SymbolValue = dataArr[31 + offset].Split(',')[33]
            };
            obj.Automobile = new
            {
                Value = dataArr[32 + offset].Split(',')[31],
                Symbol = dataArr[32 + offset].Split(',')[32],
                SymbolValue = dataArr[32 + offset].Split(',')[33]
            };
            obj.Electronics = new
            {
                Value = dataArr[33 + offset].Split(',')[31],
                Symbol = dataArr[33 + offset].Split(',')[32],
                SymbolValue = dataArr[33 + offset].Split(',')[33]
            };
            obj.BuildingMaterialAndConstruction = new
            {
                Value = dataArr[34 + offset].Split(',')[31],
                Symbol = dataArr[34 + offset].Split(',')[32],
                SymbolValue = dataArr[34 + offset].Split(',')[33]
            };
            obj.ShippingAndTransportation = new
            {
                Value = dataArr[35 + offset].Split(',')[31],
                Symbol = dataArr[35 + offset].Split(',')[32],
                SymbolValue = dataArr[35 + offset].Split(',')[33]
            };
            obj.Tourism = new
            {
                Value = dataArr[36 + offset].Split(',')[31],
                Symbol = dataArr[36 + offset].Split(',')[32],
                SymbolValue = dataArr[36 + offset].Split(',')[33]
            };
            obj.FinanceAndInsurance = new
            {
                Value = dataArr[37 + offset].Split(',')[31],
                Symbol = dataArr[37 + offset].Split(',')[32],
                SymbolValue = dataArr[37 + offset].Split(',')[33]
            };
            obj.TradingAndConsumersGoods = new
            {
                Value = dataArr[38 + offset].Split(',')[31],
                Symbol = dataArr[38 + offset].Split(',')[32],
                SymbolValue = dataArr[38 + offset].Split(',')[33]
            };
            obj.Other = new
            {
                Value = dataArr[39 + offset].Split(',')[31],
                Symbol = dataArr[39 + offset].Split(',')[32],
                SymbolValue = dataArr[39 + offset].Split(',')[33]
            };
            obj.Chemical = new
            {
                Value = dataArr[40 + offset].Split(',')[31],
                Symbol = dataArr[40 + offset].Split(',')[32],
                SymbolValue = dataArr[40 + offset].Split(',')[33]
            };
            obj.BiotechnologyAndMedicalCare = new
            {
                Value = dataArr[41 + offset].Split(',')[31],
                Symbol = dataArr[41 + offset].Split(',')[32],
                SymbolValue = dataArr[41 + offset].Split(',')[33]
            };
            obj.Semiconductor = new
            {
                Value = dataArr[42 + offset].Split(',')[31],
                Symbol = dataArr[42 + offset].Split(',')[32],
                SymbolValue = dataArr[42 + offset].Split(',')[33]
            };
            obj.ComputerAndPeripheralEquipment = new
            {
                Value = dataArr[43 + offset].Split(',')[31],
                Symbol = dataArr[43 + offset].Split(',')[32],
                SymbolValue = dataArr[43 + offset].Split(',')[33]
            };
            obj.Optoelectronic = new
            {
                Value = dataArr[44 + offset].Split(',')[31],
                Symbol = dataArr[44 + offset].Split(',')[32],
                SymbolValue = dataArr[44 + offset].Split(',')[33]
            };
            obj.CommunicationsAndInternet = new
            {
                Value = dataArr[45 + offset].Split(',')[31],
                Symbol = dataArr[45 + offset].Split(',')[32],
                SymbolValue = dataArr[45 + offset].Split(',')[33]
            };
            obj.ElectronicPartsComponents = new
            {
                Value = dataArr[46 + offset].Split(',')[31],
                Symbol = dataArr[46 + offset].Split(',')[32],
                SymbolValue = dataArr[46 + offset].Split(',')[33]
            };
            obj.ElectronicProductsDistribution = new
            {
                Value = dataArr[47 + offset].Split(',')[31],
                Symbol = dataArr[47 + offset].Split(',')[32],
                SymbolValue = dataArr[47 + offset].Split(',')[33]
            };
            obj.InformationService = new
            {
                Value = dataArr[48 + offset].Split(',')[31],
                Symbol = dataArr[48 + offset].Split(',')[32],
                SymbolValue = dataArr[48 + offset].Split(',')[33]
            };
            obj.OtherElectronic = new
            {
                Value = dataArr[49 + offset].Split(',')[31],
                Symbol = dataArr[49 + offset].Split(',')[32],
                SymbolValue = dataArr[49 + offset].Split(',')[33]
            };
            obj.OilGasAndElectricity = new
            {
                Value = dataArr[50 + offset].Split(',')[31],
                Symbol = dataArr[50 + offset].Split(',')[32],
                SymbolValue = dataArr[50 + offset].Split(',')[33]
            };
            tmpResult.Add(obj);
            return tmpResult;
        }

        private string[] ReplaceSymbol(string[] header)
        {
            List<string> tmpResult = new List<string>();

            string tmp = "";
            foreach (var data in header)
            {
                tmp = data;
                if (data.Contains("種"))
                {
                    tmp = data.Replace("種", "家");
                }
                else if (data.Contains("X"))
                {
                    tmp = data.Replace("X", "×");
                }
                else if (data.Contains("^"))
                {
                    tmp = data.Replace("^", "△");
                }
                tmpResult.Add(tmp);
            }
            return tmpResult.ToArray();
        }

        private object CheckSymbol(
            string symbolChange, string symbolClose, string symbolOpen, string symboldayHigh, string symboldayLow,
            string open, string dayHigh, string dayLow, string lastClose)
        {
            string _change = "", _close = "", _open = "", _dayHigh = "", _dayLow = "";
            double openPrice = 0, dayHighPrice = 0, dayLowPrice = 0, lastClosePrice = 0;

            try
            {
                openPrice = Convert.ToDouble(open);
                lastClosePrice = Convert.ToDouble(lastClose);
                dayHighPrice = Convert.ToDouble(dayHigh);
                dayLowPrice = Convert.ToDouble(dayLow);
            }
            catch (Exception e)
            {
                log.WarnFormat("指數轉型態時發生錯誤，Error Type: {0}", e.GetType().ToString());
            }
            switch (symbolChange)
            {
                case "^":
                    _change = "redFont";
                    _close = "redFont";
                    break;
                case "X":
                    _change = "greenFont";
                    _close = "greenFont";
                    break;
            }
            switch (symbolClose)
            {
                case "+":
                    _change = "redBg";
                    _close = "redBg";
                    break;
                case "-":
                    _change = "greenBg";
                    _close = "greenBg";
                    break;
            }

            if (openPrice > lastClosePrice)
            {
                _open = "redFont";
            }
            else if (openPrice < lastClosePrice)
            {
                _open = "greenFont";
            }
            switch (symbolOpen)
            {
                case "+":
                    _open = "redBg";
                    break;
                case "-":
                    _open = "greenBg";
                    break;
            }

            if (dayHighPrice > lastClosePrice)
            {
                _dayHigh = "redFont";
            }
            else if (dayHighPrice < lastClosePrice)
            {
                _dayHigh = "greenFont";
            }

            if (dayLowPrice > lastClosePrice)
            {
                _dayLow = "redFont";
            }
            else if (dayLowPrice < lastClosePrice)
            {
                _dayLow = "greenFont";
            }

            switch (symboldayHigh)
            {
                case "+":
                    _dayHigh = "redBg";
                    break;
                case "-":
                    _dayHigh = "greenBg";
                    break;
            }

            switch (symboldayLow)
            {
                case "+":
                    _dayLow = "redBg";
                    break;
                case "-":
                    _dayLow = "greenBg";
                    break;
            }

            var tmpResult = new
            {
                Change = (_change == "") ? null : _change,
                Open = (_open == "") ? null : _open,
                Close = (_close == "") ? null : _close,
                DayHigh = (_dayHigh == "") ? null : _dayHigh,
                DayLow = (_dayLow == "") ? null : _dayLow
            };

            return tmpResult;
        }
    }
}
