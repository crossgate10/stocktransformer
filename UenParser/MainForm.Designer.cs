﻿namespace UenParser
{
    partial class MainForm
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.listBoxProcessInfo = new System.Windows.Forms.ListBox();
            this.TypeGroup = new System.Windows.Forms.GroupBox();
            this.cklMarkets = new System.Windows.Forms.CheckedListBox();
            this.ExchangePathGroup = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnExchangePath_content = new System.Windows.Forms.Button();
            this.txtExchangePathContent = new System.Windows.Forms.TextBox();
            this.btnExchangePath_header = new System.Windows.Forms.Button();
            this.txtExchangePathHeader = new System.Windows.Forms.TextBox();
            this.btnExecuteProcess = new System.Windows.Forms.Button();
            this.BtnGroup = new System.Windows.Forms.GroupBox();
            this.btnGenerateQRCode = new System.Windows.Forms.Button();
            this.OTCPathGroup = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtOTCPathSummary = new System.Windows.Forms.TextBox();
            this.btnOTCPath_content = new System.Windows.Forms.Button();
            this.txtOTCPathContent = new System.Windows.Forms.TextBox();
            this.btnOTCPath_summary = new System.Windows.Forms.Button();
            this.btnOTCPath_header = new System.Windows.Forms.Button();
            this.txtOTCPathHeader = new System.Windows.Forms.TextBox();
            this.EmergingPathGroup = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnEmergingPath_content = new System.Windows.Forms.Button();
            this.txtEmergingPathContent = new System.Windows.Forms.TextBox();
            this.lblTimer = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.TypeGroup.SuspendLayout();
            this.ExchangePathGroup.SuspendLayout();
            this.BtnGroup.SuspendLayout();
            this.OTCPathGroup.SuspendLayout();
            this.EmergingPathGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBoxProcessInfo
            // 
            this.listBoxProcessInfo.BackColor = System.Drawing.Color.White;
            this.listBoxProcessInfo.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.listBoxProcessInfo.FormattingEnabled = true;
            this.listBoxProcessInfo.ItemHeight = 20;
            this.listBoxProcessInfo.Items.AddRange(new object[] {
            "狀態: 待命中"});
            this.listBoxProcessInfo.Location = new System.Drawing.Point(12, 437);
            this.listBoxProcessInfo.Name = "listBoxProcessInfo";
            this.listBoxProcessInfo.Size = new System.Drawing.Size(434, 244);
            this.listBoxProcessInfo.TabIndex = 0;
            // 
            // TypeGroup
            // 
            this.TypeGroup.Controls.Add(this.cklMarkets);
            this.TypeGroup.Font = new System.Drawing.Font("標楷體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.TypeGroup.Location = new System.Drawing.Point(12, 12);
            this.TypeGroup.Name = "TypeGroup";
            this.TypeGroup.Size = new System.Drawing.Size(107, 127);
            this.TypeGroup.TabIndex = 1;
            this.TypeGroup.TabStop = false;
            this.TypeGroup.Text = "市場種類";
            // 
            // cklMarkets
            // 
            this.cklMarkets.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(255)))), ((int)(((byte)(189)))));
            this.cklMarkets.CheckOnClick = true;
            this.cklMarkets.Font = new System.Drawing.Font("標楷體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.cklMarkets.FormattingEnabled = true;
            this.cklMarkets.Items.AddRange(new object[] {
            "每日上市",
            "每日上櫃",
            "每日興櫃"});
            this.cklMarkets.Location = new System.Drawing.Point(6, 26);
            this.cklMarkets.Name = "cklMarkets";
            this.cklMarkets.Size = new System.Drawing.Size(95, 92);
            this.cklMarkets.TabIndex = 0;
            // 
            // ExchangePathGroup
            // 
            this.ExchangePathGroup.Controls.Add(this.label2);
            this.ExchangePathGroup.Controls.Add(this.label1);
            this.ExchangePathGroup.Controls.Add(this.btnExchangePath_content);
            this.ExchangePathGroup.Controls.Add(this.txtExchangePathContent);
            this.ExchangePathGroup.Controls.Add(this.btnExchangePath_header);
            this.ExchangePathGroup.Controls.Add(this.txtExchangePathHeader);
            this.ExchangePathGroup.Font = new System.Drawing.Font("標楷體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ExchangePathGroup.Location = new System.Drawing.Point(12, 145);
            this.ExchangePathGroup.Name = "ExchangePathGroup";
            this.ExchangePathGroup.Size = new System.Drawing.Size(434, 86);
            this.ExchangePathGroup.TabIndex = 2;
            this.ExchangePathGroup.TabStop = false;
            this.ExchangePathGroup.Text = "上市來源檔路徑";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "Nstock4.prn";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Nstock0.prn";
            // 
            // btnExchangePath_content
            // 
            this.btnExchangePath_content.Font = new System.Drawing.Font("標楷體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnExchangePath_content.Location = new System.Drawing.Point(379, 55);
            this.btnExchangePath_content.Name = "btnExchangePath_content";
            this.btnExchangePath_content.Size = new System.Drawing.Size(48, 22);
            this.btnExchangePath_content.TabIndex = 3;
            this.btnExchangePath_content.Text = "瀏覽";
            this.btnExchangePath_content.UseVisualStyleBackColor = true;
            this.btnExchangePath_content.Click += new System.EventHandler(this.GetInputPath);
            // 
            // txtExchangePathContent
            // 
            this.txtExchangePathContent.Font = new System.Drawing.Font("標楷體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtExchangePathContent.Location = new System.Drawing.Point(132, 55);
            this.txtExchangePathContent.Name = "txtExchangePathContent";
            this.txtExchangePathContent.Size = new System.Drawing.Size(241, 27);
            this.txtExchangePathContent.TabIndex = 2;
            this.txtExchangePathContent.TextChanged += new System.EventHandler(this.TextBoxFocusOnLast);
            // 
            // btnExchangePath_header
            // 
            this.btnExchangePath_header.Font = new System.Drawing.Font("標楷體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnExchangePath_header.Location = new System.Drawing.Point(379, 26);
            this.btnExchangePath_header.Name = "btnExchangePath_header";
            this.btnExchangePath_header.Size = new System.Drawing.Size(48, 22);
            this.btnExchangePath_header.TabIndex = 1;
            this.btnExchangePath_header.Text = "瀏覽";
            this.btnExchangePath_header.UseVisualStyleBackColor = true;
            this.btnExchangePath_header.Click += new System.EventHandler(this.GetInputPath);
            // 
            // txtExchangePathHeader
            // 
            this.txtExchangePathHeader.Location = new System.Drawing.Point(132, 26);
            this.txtExchangePathHeader.Name = "txtExchangePathHeader";
            this.txtExchangePathHeader.Size = new System.Drawing.Size(241, 27);
            this.txtExchangePathHeader.TabIndex = 0;
            this.txtExchangePathHeader.TextChanged += new System.EventHandler(this.TextBoxFocusOnLast);
            // 
            // btnExecuteProcess
            // 
            this.btnExecuteProcess.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(22)))), ((int)(((byte)(84)))));
            this.btnExecuteProcess.Font = new System.Drawing.Font("標楷體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnExecuteProcess.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(204)))));
            this.btnExecuteProcess.Location = new System.Drawing.Point(6, 21);
            this.btnExecuteProcess.Name = "btnExecuteProcess";
            this.btnExecuteProcess.Size = new System.Drawing.Size(115, 34);
            this.btnExecuteProcess.TabIndex = 0;
            this.btnExecuteProcess.Text = "執行轉換";
            this.btnExecuteProcess.UseVisualStyleBackColor = false;
            this.btnExecuteProcess.Click += new System.EventHandler(this.btnExecuteProcess_Click);
            // 
            // BtnGroup
            // 
            this.BtnGroup.Controls.Add(this.btnGenerateQRCode);
            this.BtnGroup.Controls.Add(this.btnExecuteProcess);
            this.BtnGroup.Location = new System.Drawing.Point(138, 12);
            this.BtnGroup.Name = "BtnGroup";
            this.BtnGroup.Size = new System.Drawing.Size(308, 127);
            this.BtnGroup.TabIndex = 3;
            this.BtnGroup.TabStop = false;
            // 
            // btnGenerateQRCode
            // 
            this.btnGenerateQRCode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(22)))), ((int)(((byte)(84)))));
            this.btnGenerateQRCode.Font = new System.Drawing.Font("標楷體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnGenerateQRCode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(204)))));
            this.btnGenerateQRCode.Location = new System.Drawing.Point(127, 21);
            this.btnGenerateQRCode.Name = "btnGenerateQRCode";
            this.btnGenerateQRCode.Size = new System.Drawing.Size(138, 34);
            this.btnGenerateQRCode.TabIndex = 1;
            this.btnGenerateQRCode.Text = "產生QRCode";
            this.btnGenerateQRCode.UseVisualStyleBackColor = false;
            this.btnGenerateQRCode.Click += new System.EventHandler(this.btnGenerateQRCode_Click);
            // 
            // OTCPathGroup
            // 
            this.OTCPathGroup.Controls.Add(this.label5);
            this.OTCPathGroup.Controls.Add(this.label4);
            this.OTCPathGroup.Controls.Add(this.label3);
            this.OTCPathGroup.Controls.Add(this.txtOTCPathSummary);
            this.OTCPathGroup.Controls.Add(this.btnOTCPath_content);
            this.OTCPathGroup.Controls.Add(this.txtOTCPathContent);
            this.OTCPathGroup.Controls.Add(this.btnOTCPath_summary);
            this.OTCPathGroup.Controls.Add(this.btnOTCPath_header);
            this.OTCPathGroup.Controls.Add(this.txtOTCPathHeader);
            this.OTCPathGroup.Font = new System.Drawing.Font("標楷體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.OTCPathGroup.Location = new System.Drawing.Point(12, 237);
            this.OTCPathGroup.Name = "OTCPathGroup";
            this.OTCPathGroup.Size = new System.Drawing.Size(434, 127);
            this.OTCPathGroup.TabIndex = 4;
            this.OTCPathGroup.TabStop = false;
            this.OTCPathGroup.Text = "上櫃來源檔路徑";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 90);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(152, 16);
            this.label5.TabIndex = 8;
            this.label5.Text = "STKT1SUMMARY.TXT";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(152, 16);
            this.label4.TabIndex = 7;
            this.label4.Text = "STKT1QUOTESN.TXT";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "STKSECTINX.TXT";
            // 
            // txtOTCPathSummary
            // 
            this.txtOTCPathSummary.Location = new System.Drawing.Point(164, 86);
            this.txtOTCPathSummary.Name = "txtOTCPathSummary";
            this.txtOTCPathSummary.Size = new System.Drawing.Size(209, 27);
            this.txtOTCPathSummary.TabIndex = 2;
            this.txtOTCPathSummary.TextChanged += new System.EventHandler(this.TextBoxFocusOnLast);
            // 
            // btnOTCPath_content
            // 
            this.btnOTCPath_content.Font = new System.Drawing.Font("標楷體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnOTCPath_content.Location = new System.Drawing.Point(379, 84);
            this.btnOTCPath_content.Name = "btnOTCPath_content";
            this.btnOTCPath_content.Size = new System.Drawing.Size(48, 22);
            this.btnOTCPath_content.TabIndex = 5;
            this.btnOTCPath_content.Text = "瀏覽";
            this.btnOTCPath_content.UseVisualStyleBackColor = true;
            this.btnOTCPath_content.Click += new System.EventHandler(this.GetInputPath);
            // 
            // txtOTCPathContent
            // 
            this.txtOTCPathContent.Location = new System.Drawing.Point(164, 56);
            this.txtOTCPathContent.Name = "txtOTCPathContent";
            this.txtOTCPathContent.Size = new System.Drawing.Size(209, 27);
            this.txtOTCPathContent.TabIndex = 4;
            this.txtOTCPathContent.TextChanged += new System.EventHandler(this.TextBoxFocusOnLast);
            // 
            // btnOTCPath_summary
            // 
            this.btnOTCPath_summary.Font = new System.Drawing.Font("標楷體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnOTCPath_summary.Location = new System.Drawing.Point(379, 55);
            this.btnOTCPath_summary.Name = "btnOTCPath_summary";
            this.btnOTCPath_summary.Size = new System.Drawing.Size(48, 22);
            this.btnOTCPath_summary.TabIndex = 3;
            this.btnOTCPath_summary.Text = "瀏覽";
            this.btnOTCPath_summary.UseVisualStyleBackColor = true;
            this.btnOTCPath_summary.Click += new System.EventHandler(this.GetInputPath);
            // 
            // btnOTCPath_header
            // 
            this.btnOTCPath_header.Font = new System.Drawing.Font("標楷體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnOTCPath_header.Location = new System.Drawing.Point(379, 26);
            this.btnOTCPath_header.Name = "btnOTCPath_header";
            this.btnOTCPath_header.Size = new System.Drawing.Size(48, 22);
            this.btnOTCPath_header.TabIndex = 1;
            this.btnOTCPath_header.Text = "瀏覽";
            this.btnOTCPath_header.UseVisualStyleBackColor = true;
            this.btnOTCPath_header.Click += new System.EventHandler(this.GetInputPath);
            // 
            // txtOTCPathHeader
            // 
            this.txtOTCPathHeader.Location = new System.Drawing.Point(164, 26);
            this.txtOTCPathHeader.Name = "txtOTCPathHeader";
            this.txtOTCPathHeader.Size = new System.Drawing.Size(209, 27);
            this.txtOTCPathHeader.TabIndex = 0;
            this.txtOTCPathHeader.TextChanged += new System.EventHandler(this.TextBoxFocusOnLast);
            // 
            // EmergingPathGroup
            // 
            this.EmergingPathGroup.Controls.Add(this.label7);
            this.EmergingPathGroup.Controls.Add(this.label6);
            this.EmergingPathGroup.Controls.Add(this.btnEmergingPath_content);
            this.EmergingPathGroup.Controls.Add(this.txtEmergingPathContent);
            this.EmergingPathGroup.Font = new System.Drawing.Font("標楷體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.EmergingPathGroup.Location = new System.Drawing.Point(12, 370);
            this.EmergingPathGroup.Name = "EmergingPathGroup";
            this.EmergingPathGroup.Size = new System.Drawing.Size(434, 61);
            this.EmergingPathGroup.TabIndex = 6;
            this.EmergingPathGroup.TabStop = false;
            this.EmergingPathGroup.Text = "興櫃來源檔路徑";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 16);
            this.label7.TabIndex = 7;
            this.label7.Text = "otcEMG.xls";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 16);
            this.label6.TabIndex = 6;
            this.label6.Text = "new.csv 或";
            // 
            // btnEmergingPath_content
            // 
            this.btnEmergingPath_content.Font = new System.Drawing.Font("標楷體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnEmergingPath_content.Location = new System.Drawing.Point(379, 26);
            this.btnEmergingPath_content.Name = "btnEmergingPath_content";
            this.btnEmergingPath_content.Size = new System.Drawing.Size(48, 22);
            this.btnEmergingPath_content.TabIndex = 5;
            this.btnEmergingPath_content.Text = "瀏覽";
            this.btnEmergingPath_content.UseVisualStyleBackColor = true;
            this.btnEmergingPath_content.Click += new System.EventHandler(this.GetInputPath);
            // 
            // txtEmergingPathContent
            // 
            this.txtEmergingPathContent.Location = new System.Drawing.Point(132, 26);
            this.txtEmergingPathContent.Name = "txtEmergingPathContent";
            this.txtEmergingPathContent.Size = new System.Drawing.Size(241, 27);
            this.txtEmergingPathContent.TabIndex = 4;
            this.txtEmergingPathContent.TextChanged += new System.EventHandler(this.TextBoxFocusOnLast);
            // 
            // lblTimer
            // 
            this.lblTimer.AutoSize = true;
            this.lblTimer.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lblTimer.Location = new System.Drawing.Point(12, 684);
            this.lblTimer.Name = "lblTimer";
            this.lblTimer.Size = new System.Drawing.Size(0, 20);
            this.lblTimer.TabIndex = 7;
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(219)))), ((int)(((byte)(191)))));
            this.ClientSize = new System.Drawing.Size(455, 709);
            this.Controls.Add(this.lblTimer);
            this.Controls.Add(this.EmergingPathGroup);
            this.Controls.Add(this.TypeGroup);
            this.Controls.Add(this.OTCPathGroup);
            this.Controls.Add(this.BtnGroup);
            this.Controls.Add(this.ExchangePathGroup);
            this.Controls.Add(this.listBoxProcessInfo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "聯晚行情表資料轉JSON";
            this.TypeGroup.ResumeLayout(false);
            this.ExchangePathGroup.ResumeLayout(false);
            this.ExchangePathGroup.PerformLayout();
            this.BtnGroup.ResumeLayout(false);
            this.OTCPathGroup.ResumeLayout(false);
            this.OTCPathGroup.PerformLayout();
            this.EmergingPathGroup.ResumeLayout(false);
            this.EmergingPathGroup.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxProcessInfo;
        private System.Windows.Forms.GroupBox TypeGroup;
        private System.Windows.Forms.GroupBox ExchangePathGroup;
        private System.Windows.Forms.CheckedListBox cklMarkets;
        private System.Windows.Forms.Button btnExchangePath_content;
        private System.Windows.Forms.TextBox txtExchangePathContent;
        private System.Windows.Forms.Button btnExchangePath_header;
        private System.Windows.Forms.TextBox txtExchangePathHeader;
        private System.Windows.Forms.Button btnExecuteProcess;
        private System.Windows.Forms.GroupBox BtnGroup;
        private System.Windows.Forms.GroupBox OTCPathGroup;
        private System.Windows.Forms.Button btnOTCPath_content;
        private System.Windows.Forms.TextBox txtOTCPathContent;
        private System.Windows.Forms.Button btnOTCPath_summary;
        private System.Windows.Forms.TextBox txtOTCPathSummary;
        private System.Windows.Forms.Button btnOTCPath_header;
        private System.Windows.Forms.TextBox txtOTCPathHeader;
        private System.Windows.Forms.GroupBox EmergingPathGroup;
        private System.Windows.Forms.Button btnEmergingPath_content;
        private System.Windows.Forms.TextBox txtEmergingPathContent;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblTimer;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Button btnGenerateQRCode;
        private System.Windows.Forms.Label label7;
    }
}

