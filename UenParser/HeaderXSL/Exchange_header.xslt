﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="data">
    <div id="stockDialog" title="每日上市行情摘要" hidden="true">
      <table class="dialogTable">
        <tr >
          <td colspan="2" class="">今日收盤指數</td>
          <td colspan="2" class="table_right" ><xsl:value-of select="ClosingIndex/Value"/></td>
          <td colspan="2" class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="ClosingIndex/Symbol = '漲'">redFont table_right</xsl:if>
				<xsl:if test="ClosingIndex/Symbol = '跌'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="ClosingIndex/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td colspan="2" class="">不含金融股</td>
          <td colspan="2" class="table_right" ><xsl:value-of select="NonFinance/Value"/></td>
          <td colspan="2" class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="NonFinance/Symbol = '漲'">redFont table_right</xsl:if>
				<xsl:if test="NonFinance/Symbol = '跌'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="NonFinance/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td colspan="2" class="">不含電子股</td>
          <td colspan="2" class="table_right" ><xsl:value-of select="NonElectronics/Value"/></td>
          <td colspan="2" class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="NonElectronics/Symbol = '漲'">redFont table_right</xsl:if>
				<xsl:if test="NonElectronics/Symbol = '跌'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="NonElectronics/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td colspan="2" class="">不含金融電子電子股</td>
          <td colspan="2" class="table_right" ><xsl:value-of select="NonFinanceElectronics/Value"/></td>
          <td colspan="2" class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="NonFinanceElectronics/Symbol = '漲'">redFont table_right</xsl:if>
				<xsl:if test="NonFinanceElectronics/Symbol = '跌'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="NonFinanceElectronics/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td colspan="2" class="">成交總值</td>
          <td colspan="4" class="table_center"><xsl:value-of select="DealAmount"/></td>
        </tr>
        <tr>
          <td colspan="2" class="">本日最高指數</td>
          <td colspan="2" class="table_right" ><xsl:value-of select="DayHighIndex/Value"/></td>
          <td colspan="2" class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="DayHighIndex/Symbol = '漲'">redFont table_right</xsl:if>
				<xsl:if test="DayHighIndex/Symbol = '跌'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="DayHighIndex/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td colspan="2" class="">本日最低指數</td>
          <td colspan="2" class="table_right" ><xsl:value-of select="DayLowIndex/Value"/></td>
          <td colspan="2" class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="DayLowIndex/Symbol = '漲'">redFont table_right</xsl:if>
				<xsl:if test="DayLowIndex/Symbol = '跌'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="DayLowIndex/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td colspan="2" class="">台灣五十指數</td>
          <td colspan="2" class="table_right" ><xsl:value-of select="TW50Index/Value"/></td>
          <td colspan="2" class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="TW50Index/Symbol = '漲'">redFont table_right</xsl:if>
				<xsl:if test="TW50Index/Symbol = '跌'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="TW50Index/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td colspan="2" class="">寶島指數</td>
          <td colspan="2" class="table_right" ><xsl:value-of select="FormosaIndex/Value"/></td>
          <td colspan="2" class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="FormosaIndex/Symbol = '漲'">redFont table_right</xsl:if>
				<xsl:if test="FormosaIndex/Symbol = '跌'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="FormosaIndex/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td colspan="2" class="">高薪100指數</td>
          <td colspan="2" class="table_right" ><xsl:value-of select="SalaryHigh100Index/Value"/></td>
          <td colspan="2" class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="SalaryHigh100Index/Symbol = '漲'">redFont table_right</xsl:if>
				<xsl:if test="SalaryHigh100Index/Symbol = '跌'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="SalaryHigh100Index/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td colspan="2" class="">電子類報酬指數</td>
          <td colspan="2" class="table_right" ><xsl:value-of select="ElectronicsRewardIndex/Value"/></td>
          <td colspan="2" class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="ElectronicsRewardIndex/Symbol = '漲'">redFont table_right</xsl:if>
				<xsl:if test="ElectronicsRewardIndex/Symbol = '跌'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="ElectronicsRewardIndex/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td colspan="2" class="">金融保險報酬數</td>
          <td colspan="2" class="table_right" ><xsl:value-of select="FinanceRewardIndex/Value"/></td>
          <td colspan="2" class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="FinanceRewardIndex/Symbol = '漲'">redFont table_right</xsl:if>
				<xsl:if test="FinanceRewardIndex/Symbol = '跌'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="FinanceRewardIndex/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td class="">水泥窯製類</td>
          <td class="table_right" ><xsl:value-of select="CementAndCeramic/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="CementAndCeramic/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="CementAndCeramic/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="CementAndCeramic/SymbolValue"/>
		  </td>
          <td class="">塑膠化工類</td>
          <td class="table_right" ><xsl:value-of select="PlasticAndChemical/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="PlasticAndChemical/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="PlasticAndChemical/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="PlasticAndChemical/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td class="">機電類</td>
          <td class="table_right" ><xsl:value-of select="Electrical/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="Electrical/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="Electrical/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="Electrical/SymbolValue"/>
		  </td>
          <td class="">水泥類</td>
          <td class="table_right" ><xsl:value-of select="Cement/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="Cement/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="Cement/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="Cement/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td class="">食品類</td>
          <td class="table_right" ><xsl:value-of select="Food/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="Food/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="Food/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="Food/SymbolValue"/>
		  </td>
          <td class="">塑膠類</td>
          <td class="table_right" ><xsl:value-of select="Plastic/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="Plastic/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="Plastic/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="Plastic/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td class="">紡織纖維類</td>
          <td class="table_right" ><xsl:value-of select="Textile/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="Textile/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="Textile/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="Textile/SymbolValue"/>
		  </td>
          <td class="">電機機械類</td>
          <td class="table_right" ><xsl:value-of select="ElectricMachinery/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="ElectricMachinery/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="ElectricMachinery/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="ElectricMachinery/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td class="">電器電纜類</td>
          <td class="table_right" ><xsl:value-of select="ElectricalAndCable/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="ElectricalAndCable/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="ElectricalAndCable/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="ElectricalAndCable/SymbolValue"/>
		  </td>
          <td class="">化工生醫類</td>
          <td class="table_right" ><xsl:value-of select="ChemicalBiotechnologyAndMedicalCare/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="ChemicalBiotechnologyAndMedicalCare/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="ChemicalBiotechnologyAndMedicalCare/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="ChemicalBiotechnologyAndMedicalCare/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td class="">玻璃陶瓷類</td>
          <td class="table_right" ><xsl:value-of select="GlassAndCeramic/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="GlassAndCeramic/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="GlassAndCeramic/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="GlassAndCeramic/SymbolValue"/>
		  </td>
          <td class="">造紙類</td>
          <td class="table_right" ><xsl:value-of select="PaperAndPulp/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="PaperAndPulp/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="PaperAndPulp/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="PaperAndPulp/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td class="">鋼鐵類</td>
          <td class="table_right" ><xsl:value-of select="IronAndSteel/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="IronAndSteel/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="IronAndSteel/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="IronAndSteel/SymbolValue"/>
		  </td>
          <td class="">橡膠類</td>
          <td class="table_right" ><xsl:value-of select="Rubber/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="Rubber/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="Rubber/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="Rubber/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td class="">汽車類</td>
          <td class="table_right" ><xsl:value-of select="Automobile/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="Automobile/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="Automobile/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="Automobile/SymbolValue"/>
		  </td>
          <td class="">電子類</td>
          <td class="table_right" ><xsl:value-of select="Electronics/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="Electronics/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="Electronics/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="Electronics/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td class="">營造材料類</td>
          <td class="table_right" ><xsl:value-of select="BuildingMaterialAndConstruction/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="BuildingMaterialAndConstruction/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="BuildingMaterialAndConstruction/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="BuildingMaterialAndConstruction/SymbolValue"/>
		  </td>
          <td class="">航運類</td>
          <td class="table_right" ><xsl:value-of select="ShippingAndTransportation/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="ShippingAndTransportation/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="ShippingAndTransportation/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="ShippingAndTransportation/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td class="">觀光類</td>
          <td class="table_right" ><xsl:value-of select="Tourism/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="Tourism/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="Tourism/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="Tourism/SymbolValue"/>
		  </td>
          <td class="">金融保險類</td>
          <td class="table_right" ><xsl:value-of select="FinanceAndInsurance/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="FinanceAndInsurance/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="FinanceAndInsurance/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="FinanceAndInsurance/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td class="">百貨貿易類</td>
          <td class="table_right" ><xsl:value-of select="TradingAndConsumersGoods/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="TradingAndConsumersGoods/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="TradingAndConsumersGoods/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="TradingAndConsumersGoods/SymbolValue"/>
		  </td>
          <td class="">其他</td>
          <td class="table_right" ><xsl:value-of select="Other/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="Other/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="Other/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="Other/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td class="">化學工業類</td>
          <td class="table_right" ><xsl:value-of select="Chemical/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="Chemical/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="Chemical/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="Chemical/SymbolValue"/>
		  </td>
          <td class="">生技醫療類</td>
          <td class="table_right" ><xsl:value-of select="BiotechnologyAndMedicalCare/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="BiotechnologyAndMedicalCare/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="BiotechnologyAndMedicalCare/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="BiotechnologyAndMedicalCare/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td class="">半導體類</td>
          <td class="table_right" ><xsl:value-of select="Semiconductor/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="Semiconductor/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="Semiconductor/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="Semiconductor/SymbolValue"/>
		  </td>
          <td class="">電腦周邊類</td>
          <td class="table_right" ><xsl:value-of select="ComputerAndPeripheralEquipment/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="ComputerAndPeripheralEquipment/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="ComputerAndPeripheralEquipment/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="ComputerAndPeripheralEquipment/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td class="">光電類</td>
          <td class="table_right" ><xsl:value-of select="Optoelectronic/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="Optoelectronic/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="Optoelectronic/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="Optoelectronic/SymbolValue"/>
		  </td>
          <td class="">通訊網路類</td>
          <td class="table_right" ><xsl:value-of select="CommunicationsAndInternet/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="CommunicationsAndInternet/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="CommunicationsAndInternet/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="CommunicationsAndInternet/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td class="">電子零組件</td>
          <td class="table_right" ><xsl:value-of select="ElectronicPartsComponents/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="ElectronicPartsComponents/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="ElectronicPartsComponents/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="ElectronicPartsComponents/SymbolValue"/>
		  </td>
          <td class="">電子通訊類</td>
          <td class="table_right" ><xsl:value-of select="ElectronicProductsDistribution/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="ElectronicProductsDistribution/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="ElectronicProductsDistribution/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="ElectronicProductsDistribution/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td class="">資訊服務類</td>
          <td class="table_right" ><xsl:value-of select="InformationService/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="InformationService/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="InformationService/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="InformationService/SymbolValue"/>
		  </td>
          <td class="">其他電子類</td>
          <td class="table_right" ><xsl:value-of select="OtherElectronic/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="OtherElectronic/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="OtherElectronic/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="OtherElectronic/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td class="">油電燃氣類</td>
          <td class="table_right" ><xsl:value-of select="OilGasAndElectricity/Value"/></td>
          <td class="">
			<xsl:variable name="ClassVar">
				<xsl:if test="OilGasAndElectricity/Symbol = '△'">redFont table_right</xsl:if>
				<xsl:if test="OilGasAndElectricity/Symbol = '×'">greenFont table_right</xsl:if>
			</xsl:variable>
            <xsl:attribute name="class">
				<xsl:value-of select="normalize-space($ClassVar)"/>
			</xsl:attribute>
			<xsl:value-of select="OilGasAndElectricity/SymbolValue"/>
		  </td>
        </tr>
        <tr>
          <td colspan="6" class="table_center">股票29日除權交易參考價為:國精化31.48元</td>
        </tr>
        <tr>
          <td class="">漲停</td>
          <td class="table_right"><xsl:value-of select="RiseFallAmount/RiseLimit"/></td>
          <td class="">跌停</td>
          <td class="table_right"><xsl:value-of select="RiseFallAmount/FallLimit"/></td>
          <td class="">漲</td>
          <td class="table_right"><xsl:value-of select="RiseFallAmount/Rise"/></td>
        </tr>
        <tr>
          <td class="">跌</td>
          <td class="table_right"><xsl:value-of select="RiseFallAmount/Fall"/></td>
          <td class="">平</td>
          <td class="table_right"><xsl:value-of select="RiseFallAmount/Same"/></td>
          <td class="">未成交</td>
          <td class="table_right"><xsl:value-of select="RiseFallAmount/NonDeal"/></td>
        </tr>
        <tr>
          <td class="">無比價</td>
          <td class="table_right"><xsl:value-of select="RiseFallAmount/NonComparePrice"/></td>
          <td colspan="4" class="table_center">本表由交易所，寶碩資訊電腦連線製作</td>
        </tr>
      </table>
    </div>
  </xsl:template>
</xsl:stylesheet>
