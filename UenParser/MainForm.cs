﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using UenParser.Generators;

namespace UenParser
{
    public partial class MainForm : Form
    {
        private static ILog log = LogManager.GetLogger(typeof(MainForm));
        private string baseUrl = "http://udn.com/uen/";
        private static string baseFolder = AppDomain.CurrentDomain.BaseDirectory;
        private string packFolder = baseFolder + "Pack\\";
        private const string exchangeHeaderFileName = "Exchange_header.js";
        private const string otcHeaderFileName = "OTC_header.js";
        
        public MainForm()
        {
            XmlConfigurator.Configure(new FileInfo(baseFolder + "log4net.properties"));
            InitializeComponent();
            InitializeCheckListBox();
            BackupFiles("Output", true);
            LoadSourceFilePaths();
        }

        private void LoadSourceFilePaths()
        {
            //每日上市
            txtExchangePathHeader.Text = baseFolder + "Source\\Nstock0.prn";
            txtExchangePathContent.Text = baseFolder + "Source\\Nstock4.prn";

            //每日上櫃
            txtOTCPathHeader.Text = baseFolder + "Source\\STKSECTINX.TXT";
            txtOTCPathContent.Text = baseFolder + "Source\\STKT1QUOTESN.TXT";

            //每日興櫃
            txtEmergingPathContent.Text = baseFolder + "Source\\otcEMG.xls";
        }

        private void InitializeCheckListBox()
        {
            for (int i = 0; i < cklMarkets.Items.Count; i++)
            {
                cklMarkets.SetItemChecked(i, true);
            }
        }

        private void BackupFiles(string backupType, bool isTodayFirstExecute)
        {
            string dateNow = DateTime.Now.ToString("yyyyMMdd");

            string sourceDirPath = baseFolder + backupType + "\\";
            string backupDirPath = baseFolder + backupType + "Backup\\";
            string dateNowPath = backupDirPath + dateNow;
            DirectoryInfo sourceDir = new DirectoryInfo(sourceDirPath);
            FileInfo[] sourceFileInfo = sourceDir.GetFiles();
            DirectoryInfo backupDir = new DirectoryInfo(backupDirPath);

            int backupAmount = 0;
            if (!Directory.Exists(dateNowPath))
            {
                Directory.CreateDirectory(dateNowPath);
                if (!isTodayFirstExecute)
                {
                    log.InfoFormat("開始備份檔案，備份種類: {0}", backupType);
                    backupAmount = DoCopyBackupFiles(sourceFileInfo, sourceDirPath, dateNowPath + "\\", dateNow);
                }
            }
            else
            {
                log.InfoFormat("開始備份檔案，備份種類: {0}", backupType);
                backupAmount = DoCopyBackupFiles(sourceFileInfo, sourceDirPath, dateNowPath + "\\", dateNow);
            }

            if (backupAmount == 0)
            {
                if (!isTodayFirstExecute)
                {
                    ProcessInfoAdd(backupType + " 資料夾 今日已備份!");
                    log.InfoFormat("{0} 資料夾 今日已備份!", backupType);
                }
            }
            else
            {
                ProcessInfoAdd(backupType + " 資料夾備份完成，共 " + backupAmount + " 個檔案!");
                log.InfoFormat("{0} 資料夾備份完成，共 {1} 個檔案!", backupType, backupAmount);
            }
        }

        private int DoCopyBackupFiles(FileInfo[] sourceFileInfo, string sourcePath, string destPath, string dateNow)
        {
            string extension = "", orginalFileName = "", backupFileName = "";
            int fileCopyAmount = 0;
            foreach (FileInfo foundFile in sourceFileInfo)
            {
                extension = foundFile.Extension;
                orginalFileName = foundFile.Name.Split('.')[0];
                backupFileName = dateNow != "" ? orginalFileName + "_" + dateNow : orginalFileName;
                try
                {
                    File.Copy((sourcePath + orginalFileName + extension), (destPath + backupFileName + extension));
                    fileCopyAmount++;
                }
                catch (Exception ex)
                {
                    log.WarnFormat("警告: 檔案複製時遇到問題. 訊息: {0}", ex.Message);
                }
            }
            return fileCopyAmount;
        }

        private void CopyFolder(string SourcePath, string DestinationPath)
        {
            //Now Create all of the directories
            foreach (string dirPath in Directory.GetDirectories(SourcePath, "*",
                SearchOption.AllDirectories))
                Directory.CreateDirectory(dirPath.Replace(SourcePath, DestinationPath));

            //Copy all the files & Replaces any files with the same name
            foreach (string newPath in Directory.GetFiles(SourcePath, "*.*",
                SearchOption.AllDirectories))
                File.Copy(newPath, newPath.Replace(SourcePath, DestinationPath), true);
        }

        private void GetInputPath(object sender, EventArgs e)
        {
            Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            Button btn = (Button)sender;
                            switch (btn.Name)
                            {
                                case "btnExchangePath_header":
                                    txtExchangePathHeader.Text = openFileDialog1.FileName;
                                    break;
                                case "btnExchangePath_content":
                                    txtExchangePathContent.Text = openFileDialog1.FileName;
                                    break;
                                case "btnOTCPath_header":
                                    txtOTCPathHeader.Text = openFileDialog1.FileName;
                                    break;
                                case "btnOTCPath_summary":
                                    txtOTCPathSummary.Text = openFileDialog1.FileName;
                                    break;
                                case "btnOTCPath_content":
                                    txtOTCPathContent.Text = openFileDialog1.FileName;
                                    break;
                                case "btnEmergingPath_content":
                                    txtEmergingPathContent.Text = openFileDialog1.FileName;
                                    break;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.ErrorFormat("錯誤: 無法讀取檔案. 訊息: {0}", ex.Message);
                }
            }
        }

        private void AppendHeaderInfoIntoHTML(FileInfo file, string sourcePath, string headerPath)
        {
            string htmlPath = sourcePath + "index.html";
            string[] htmlContent = File.ReadAllLines(htmlPath);
            string headerData = new StreamReader(headerPath + file.Name).ReadToEnd();

            try
            {
                switch (file.Name)
                {
                    case exchangeHeaderFileName:
                        htmlContent[31] = headerData;
                        break;
                    case otcHeaderFileName:
                        htmlContent[32] = headerData;
                        break;
                }
                File.WriteAllLines(sourcePath, htmlContent);
            }
            catch (Exception ex)
            {
                log.ErrorFormat("錯誤: 表頭資料寫入index.html失敗. 訊息: {0}", ex.Message);
            }
            var tmp = "";
        }

        private void MergeAllData(string sourcePath, string destPath)
        {
            DirectoryInfo sourceDir = new DirectoryInfo(sourcePath);
            FileInfo[] sourceFileInfo = sourceDir.GetFiles();
            List<string[]> contents = new List<string[]>();

            try
            {
                foreach (var file in sourceFileInfo)
                {
                    if (file.Extension == ".js" && file.Name != exchangeHeaderFileName && file.Name != otcHeaderFileName)
                    {
                        contents.Add(File.ReadAllLines(file.FullName));
                    }
                    else if (file.Extension == ".png")
                    {
                        File.Copy(file.FullName, file.FullName.Replace(sourcePath, destPath), true);
                    }
                    else
                    {
                        switch(file.Name)
                        {
                            case exchangeHeaderFileName:
                                AppendHeaderInfoIntoHTML(file, destPath, sourcePath);
                                break;
                            case otcHeaderFileName:
                                AppendHeaderInfoIntoHTML(file, destPath, sourcePath);
                                break;
                        }
                    }
                }
                using (StreamWriter sw = File.CreateText(destPath + "json\\" + "data.js"))
                {
                    foreach (var content in contents)
                        sw.WriteLine(content[0]);
                }
            }
            catch (Exception ex)
            {
                log.ErrorFormat("錯誤: 資料合併失敗. 訊息: {0}", ex.Message);
            }  
        }

        private void ProduceGenerateMarketPrice()
        {
            foreach (int item in cklMarkets.CheckedIndices)
            {
                List<string> paths = new List<string>();
                switch (item)
                {
                    case 0:
                        log.Info("開始轉換上市檔案");
                        ExchangePriceGenerator exchange = new ExchangePriceGenerator();
                        paths.Add(txtExchangePathHeader.Text);
                        paths.Add(txtExchangePathContent.Text);
                        exchange.ProduceOutput(exchange.GenerateJson(paths), "Exchange");
                        ProcessInfoAdd(DateTime.Now.ToString() + " " + "上市JS產生完成");
                        log.Info("上市JS產生完成");
                        break;
                    case 1:
                        log.Info("開始轉換上櫃檔案");
                        OTCPriceGenerator otc = new OTCPriceGenerator();
                        paths.Add(txtOTCPathHeader.Text);
                        paths.Add(txtOTCPathContent.Text);
                        otc.ProduceOutput(otc.GenerateJson(paths), "OTC");
                        ProcessInfoAdd(DateTime.Now.ToString() + " " + "上櫃JS產生完成");
                        log.Info("上櫃JS產生完成");
                        break;
                    case 2:
                        log.Info("開始轉換興櫃檔案");
                        EmergingPriceGenerator emerging = new EmergingPriceGenerator();
                        paths.Add(txtEmergingPathContent.Text);
                        emerging.ProduceOutput(emerging.GenerateJson(paths), "Emerging");
                        ProcessInfoAdd(DateTime.Now.ToString() + " " + "興櫃JS產生完成");
                        log.Info("興櫃JS產生完成");
                        break;
                }
            }
        }

        private void PackDataIntoFolder()
        {
            DateTime today = DateTime.Now;
            string destPath = packFolder + today.ToString("yyyyMMdd") + "\\";
            string htmlTemplateSourcePath = baseFolder + "HTMLTemplate\\";
            string dataJsSourcePath = baseFolder + "Output\\";
            try
            {
                CopyFolder(htmlTemplateSourcePath, destPath);
                MergeAllData(dataJsSourcePath, destPath);
                ProcessInfoAdd("所有資料打包完成! 請將PDF檔放至pdf資料夾下");
            }
            catch (Exception ex)
            {
                ProcessInfoAdd("資料打包失敗! 請檢查log");
                log.ErrorFormat("錯誤: 資料打包失敗. 訊息: {0}", ex.Message);
            }
        }

        private void ExcecuteProcess()
        {
            string sourcePath = baseFolder + "Output\\";
            string destPath = packFolder + DateTime.Now.ToString("yyyyMMdd") + "\\";
            
            ProduceGenerateMarketPrice();
            IndexHTMLGenerator.ProduceOutput(sourcePath, destPath);
            PackDataIntoFolder();
            BackupFiles("Source", false);
            BackupFiles("Output", false);
        }

        #region SetTimeout/ClearTimeout Simulation
        //Dictionary for running setTimeout
        static Dictionary<Guid, Thread> _setTimeoutHandles =
            new Dictionary<Guid, Thread>();
        //SetTimeout for no UI Thread issue
        static Guid SetTimeout(Action cb, int delay)
        {
            return SetTimeout(cb, delay, null);
        }
        //Javascript-style SetTimeout function
        //remember to set uiForm argument when there cb is trying
        //to change UI controls in window form
        //it will return a GUID as handle for cancelling
        static Guid SetTimeout(Action cb, int delay, Form uiForm)
        {
            Guid g = Guid.NewGuid();
            Thread t = new Thread(() =>
            {
                Thread.Sleep(delay);
                _setTimeoutHandles.Remove(g);
                if (uiForm != null)
                    //use Invoke() to avoid threading issue
                    //Ref: http://tinyurl.com/yjckzhz
                    uiForm.Invoke(cb);
                else
                    cb();
            });
            _setTimeoutHandles.Add(g, t);
            t.Start();
            return g;
        }
        //Javascript-style ClearTimeout
        static void ClearTimeout(Guid g)
        {
            if (!_setTimeoutHandles.ContainsKey(g))
                return;
            _setTimeoutHandles[g].Abort();
            _setTimeoutHandles.Remove(g);
        }
        #endregion

        private void btnExecuteProcess_Click(object sender, EventArgs e)
        {
            if (btnExecuteProcess.Enabled)
            {
                ProcessInfoChangeState("正在轉換中，請稍候。。。", 100);
                SetTimeout(() => ExcecuteProcess(), 400, this);
                ProcessInfoChangeState("已完成，待命中", 800);
            }
        }

        private void ProcessInfoAdd(object info)
        {
            listBoxProcessInfo.Items.Add(info);
            listBoxProcessInfo.SetSelected(listBoxProcessInfo.Items.Count - 1, true);
        }

        private void ProcessInfoChangeState(string state, int ms)
        {
            SetTimeout(() => listBoxProcessInfo.Items[0] = "狀態: " + state, ms, this);
        }

        private void TextBoxFocusOnLast(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.SelectionStart = tb.Text.Length - 1;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            lblTimer.Text = "今天日期: " + DateTime.Now.ToString("yyyy-MM-dd") + "  現在時刻: " + DateTime.Now.ToString("HH : mm : ss.fff");
        }

        private void btnGenerateQRCode_Click(object sender, EventArgs e)
        {
            string folderName = RandomCodeGenerator.ProduceOutput();
            string destPath = packFolder + DateTime.Now.ToString("yyyyMMdd");
            if (folderName != "")
            {
                QRCodeGenerator.ProduceOutput(baseUrl + folderName + "/");
                if (Directory.Exists(destPath) != true)
                {
                    Directory.CreateDirectory(destPath);
                    File.CreateText(destPath + "\\" + folderName);
                    ProcessInfoAdd("QRCode 與今日Pack資料夾產生完成");
                }
                else
                    ProcessInfoAdd("今日Pack資料夾已存在，請檢查");
            }
            else
            {
                ProcessInfoAdd("QRCode 產生錯誤，請檢查log");
            }
        }
    }
}
