﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UenParser.JsonModel
{
    public class OTCHeder
    {
        public string Industry { get; set; }
        public string SymbolClose { get; set; }
        public string Change { get; set; }
        public string Close { get; set; }
        public string Volume { get; set; }
    }
}
