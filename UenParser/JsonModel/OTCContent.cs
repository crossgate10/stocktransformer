﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace UenParser.JsonModel
{
    public class OTCContent
    {
        public string Industry { get; set; }
        public string Code { get; set; }
        public string Company { get; set; }
        public string SymbolClose { get; set; }
        public string Change { get; set; }
        public string Close { get; set; }
        public string NextRiseLimit { get; set; }
        public string NextFallLimit { get; set; }
        public string SymbolOpen { get; set; }
        public string Open { get; set; }
        public string SymbolDayHigh { get; set; }
        public string DayHigh { get; set; }
        public string SymbolDayLow { get; set; }
        public string DayLow { get; set; }
        public string Volume { get; set; }
        public string Capital { get; set; }
        public string PE { get; set; }

        //for PQGrid Plugin
        private object _pq_cellcls;
        [ScriptIgnore]
        public object pq_cellcls
        {
            get
            {
                if (_pq_cellcls != null) return _pq_cellcls;
                else return null;
            }
            set
            {
                _pq_cellcls = value;
            }
        }
    }
}
