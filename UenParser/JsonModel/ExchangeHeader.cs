﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace UenParser.JsonModel
{
    public class ExchangeHeader
    {
        public string Date { get; set; }
        public object ClosingIndex { get; set; }
        public object NonFinance { get; set; }
        public object NonElectronics { get; set; }
        public object NonFinanceElectronics { get; set; }
        public string DealAmount { get; set; }
        public object DayHighIndex { get; set; }
        public object DayLowIndex { get; set; }
        public object TW50Index { get; set; }
        public object FormosaIndex { get; set; }
        public object SalaryHigh100Index { get; set; }
        public object ElectronicsRewardIndex { get; set; }
        public object FinanceRewardIndex { get; set; }
        public object CementAndCeramic { get; set; }
        public object PlasticAndChemical { get; set; }
        public object Electrical { get; set; }
        public object Cement { get; set; }
        public object Food { get; set; }
        public object Plastic { get; set; }
        public object Textile { get; set; }
        public object ElectricMachinery { get; set; }
        public object ElectricalAndCable { get; set; }
        public object ChemicalBiotechnologyAndMedicalCare { get; set; }
        public object GlassAndCeramic { get; set; }
        public object PaperAndPulp { get; set; }
        public object IronAndSteel { get; set; }
        public object Rubber { get; set; }
        public object Automobile { get; set; }
        public object Electronics { get; set; }
        public object BuildingMaterialAndConstruction { get; set; }
        public object ShippingAndTransportation { get; set; }
        public object Tourism { get; set; }
        public object FinanceAndInsurance { get; set; }
        public object TradingAndConsumersGoods { get; set; }
        public object Other { get; set; }
        public object Chemical { get; set; }
        public object BiotechnologyAndMedicalCare { get; set; }
        public object Semiconductor { get; set; }
        public object ComputerAndPeripheralEquipment { get; set; }
        public object Optoelectronic { get; set; }
        public object CommunicationsAndInternet { get; set; }
        public object ElectronicPartsComponents { get; set; }
        public object ElectronicProductsDistribution { get; set; }
        public object InformationService { get; set; }
        public object OtherElectronic { get; set; }
        public object OilGasAndElectricity { get; set; }
        private List<string> _exRightReferencePrice = new List<string>();
        [ScriptIgnore]
        public string[] ExRightReferencePrice 
        {
            get
            {
                if (_exRightReferencePrice.Count != 0) return _exRightReferencePrice.ToArray();
                else return null;
            }
            set
            {
                _exRightReferencePrice.Add(value[0]);
            }
        }       
        public object RiseFallAmount { get; set; }
    }
}
