﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UenParser.JsonModel
{
    public class Emerging
    {
        public string Code { get; set; }
        public string Company { get; set; }
        public string PreAveragePrice { get; set; }
        public string BuyingPrice { get; set; }
        public string SellingPrice { get; set; }
        public string DayHigh { get; set; }
        public string DayLow { get; set; }
        public string Close { get; set; }
        public string Volume { get; set; }
    }
}
