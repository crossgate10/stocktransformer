﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace UenParser.JsonModel
{
    public class ExchangeContent : OTCContent
    {
        private string _industry;
        [ScriptIgnore]
        public new string Industry
        {
            get
            {
                if (_industry != "") return _industry;
                else return null;
            }
            set
            {
                _industry = value;
            }
        }
        private string _symbolClose;
        [ScriptIgnore]
        public new string SymbolClose
        {
            get
            {
                if (_symbolClose != "") return _symbolClose;
                else return null;
            }
            set
            {
                _symbolClose = value;
            }
        }
        private string _symbolOpen;
        [ScriptIgnore]
        public new string SymbolOpen
        {
            get
            {
                if (_symbolOpen != "") return _symbolOpen;
                else return null;
            }
            set
            {
                _symbolOpen = value;
            }
        }
        private string _symbolDayHigh;
        [ScriptIgnore]
        public new string SymbolDayHigh
        {
            get
            {
                if (_symbolDayHigh != "") return _symbolDayHigh;
                else return null;
            }
            set
            {
                _symbolDayHigh = value;
            }
        }
        private string _symbolDayLow;
        [ScriptIgnore]
        public new string SymbolDayLow
        {
            get
            {
                if (_symbolDayLow != "") return _symbolDayLow;
                else return null;
            }
            set
            {
                _symbolDayLow = value;
            }
        }
        public string VolumePerTrade { get; set; }
        public string PreVolume { get; set; }
        public string RsiPer5 { get; set; }
        public string VolumePer5 { get; set; }
        public string PricePer5 { get; set; }
        public string PricePer20 { get; set; }
        public string BiasPer10 { get; set; }
        public string LastYearHigh { get; set; }
        public string LastYearLow { get; set; }
    }
}
