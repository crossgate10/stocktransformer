
	$(document).ready(function() {
			tableElement = $('#emergingTable');
			var table=tableElement.DataTable({
				"bProcessing": true,
				"oLanguage": {
					"sProcessing": "<div id='mask'></div>"
				},
				"ordering": false,
				"ajax": 'json/emerging.json',
				"deferRender": true,
				"columns": [
					{ "data": "company" },
					{ "data": "preAveragePrice" },
					{ "data": "buyingPrice" },
					{ "data": "sellingPrice" }
				],
				"fixedHeader": true,
				"scrollY":  '80vh',
				"scrollCollapse": true,
				"paging": false,
				"fixedColumns":   {leftColumns: 1},
				"scrollX": true,
				"sDom": '<"#emergingTable"t><"#ProcessArea"r>'				
			});
			
			$('#mySearch').on( 'click', function () {
				$('#loading').show();
				table.columns(0).search($('#myInput').val()).draw();
				setTimeout(function(){$('#loading').hide();}, 3000);
			});
			
			$('#select-choice-mini').on('change',function(){
				table.columns(12).search($(this).val()).draw();
			});
			
			$(window).resize(function() {
				//location.reload();
				$('#loading').show();
				table.draw();
				setTimeout(function(){$('#loading').hide();}, 3000);
			})
		});
			
		