
	$(document).ready(function() {
			tableElement = $('#otcTable');
			var table=tableElement.DataTable({
				"bProcessing": true,
				"oLanguage": {
					"sProcessing": "<div id='mask'></div>"
				},
				"ordering": false,
				"ajax": 'json/otc.json',
				"deferRender": true,
				"columns": [
					{ "data": "code" },
					{ "data": "company" },
					{ "data": "change" },
					{ "data": "close" },
					{ "data": "nextRiseLimit" },
					{ "data": "nextFallLimit" },
					{ "data": "open" },
					{ "data": "dayHigh" },
					{ "data": "dayLow" },
					{ "data": "volume" },
					{ "data": "PE" },
					{ "data": "capital" },
					{ "data": "industry" }
				],
				 "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
					switch (aData.symbolClose) {
					case "^":
						$('td:eq(2)', nRow).addClass("redBg");
						$('td:eq(3)', nRow).addClass("redBg");
						break;
					case "v":
						$('td:eq(2)', nRow).addClass("greenBg");
						$('td:eq(3)', nRow).addClass("greenBg");
						break;
					case "+":
						$('td:eq(2)', nRow).addClass("redFont");
						$('td:eq(3)', nRow).addClass("redFont");
						$('td:eq(7)', nRow).addClass("redFont");
						break;
					case "-":
						$('td:eq(2)', nRow).addClass("greenFont");
						$('td:eq(3)', nRow).addClass("greenFont");
						$('td:eq(8)', nRow).addClass("greenFont");
						break;
					}
				},
				"fixedHeader": true,
				"scrollY":  '80vh',
				"scrollCollapse": true,
				"paging": false,
				"fixedColumns":   {leftColumns: 2},
				"scrollX": true,
				"sDom": '<"#otcTable"t><"#ProcessArea"r>'				
			});
			
			$('#mySearch').on( 'click', function () {
				$('#loading').show();
				table.columns(0).search($('#myInput').val()).draw();
				setTimeout(function(){$('#loading').hide();}, 3000);
			});
			
			$('#select-choice-mini').on('change',function(){
				table.columns(12).search($(this).val()).draw();
			});
			
			$(window).resize(function() {
				//location.reload();
				$('#loading').show();
				table.draw();
				setTimeout(function(){$('#loading').hide();}, 3000);
			})
		});
			
		