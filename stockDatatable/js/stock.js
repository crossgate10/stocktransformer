

	$(document).ready(function() {
		//alert('h:'+$(window).height()+" w:"+$(window).width());
		//alert('h:'+screen.height+" w:"+screen.width);
			tableElement = $('#stockTable');
			var table=tableElement.DataTable({
				"bProcessing": true,
				"oLanguage": {
					"sProcessing": "<div id='mask'></div>"
				},
				"ordering": false,
				//"ServerSide": true,
				//"ajax": '/loadJson.php',
				"ajax": 'json/stock.json',
				"deferRender": true,
				"columns": [
					{ "data": "code" },
					{ "data": "company" },
					{ "data": "change" },
					{ "data": "close" },
					{ "data": "nextRiseLimit" },
					{ "data": "nextFallLimit" },
					{ "data": "open" },
					{ "data": "dayHigh" },
					{ "data": "dayLow" },
					{ "data": "volumePerTrade" },
					{ "data": "volume" },
					{ "data": "preVolume" },
					{ "data": "RsiPer5" },
					{ "data": "volumePer5" },
					{ "data": "pricePer5" },
					{ "data": "pricePer20" },
					{ "data": "BiasPer10" },
					{ "data": "lastYearHigh" },
					{ "data": "lastYearLow" },
					{ "data": "PE" },
					{ "data": "capital" },
					{ "data": "industry" }
				],
				 "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
					switch (aData.symbolClose) {
					case "^":
						//$('td:eq(2)', nRow).addClass("text-danger");
						$('td:eq(2)', nRow).addClass("redFont");
						$('td:eq(3)', nRow).addClass("redFont");
						$('td:eq(7)', nRow).addClass("redFont");
						break;
					case "X":
						//$('td:eq(2)', nRow).addClass("text-success");
						$('td:eq(2)', nRow).addClass("greenFont");
						$('td:eq(3)', nRow).addClass("greenFont");
						$('td:eq(8)', nRow).addClass("greenFont");
						break;
					case "+":
						//$('td:eq(2)', nRow).addClass("table-danger");
						$('td:eq(2)', nRow).addClass("redBg");
						$('td:eq(3)', nRow).addClass("redBg");
						break;
					case "-":
						$('td:eq(2)', nRow).addClass("greenBg");
						$('td:eq(3)', nRow).addClass("greenBg");
						break;
					}
					
					switch (aData.symbolOpen) {
					case "+":
						$('td:eq(6)', nRow).addClass("redBg");
						break;
					case "-":
						$('td:eq(6)', nRow).addClass("greenBg");
						break;
					}
					
					switch (aData.symboldayHigh) {
					case "+":
						$('td:eq(7)', nRow).removeClass("redFont");
						$('td:eq(7)', nRow).addClass("redBg");
						break;
					case "-":
						$('td:eq(7)', nRow).removeClass("redFont");
						$('td:eq(7)', nRow).addClass("greenBg");
						break;
					}
					
					switch (aData.symboldayLow) {
					case "+":
						$('td:eq(8)', nRow).removeClass("greenFont");
						$('td:eq(8)', nRow).addClass("redBg");
						break;
					case "-":
						$('td:eq(8)', nRow).removeClass("greenFont");
						$('td:eq(8)', nRow).addClass("greenBg");
						break;
					}
				},
				"fixedHeader": true,
				"scrollY":  '80vh',
				"scrollCollapse": true,
				"paging": false,
				"fixedColumns":   {leftColumns: 2},
				"scrollX": true,
				"sDom": '<"#stockTable"t><"#ProcessArea"r>'				
			});
			
			$('#mySearch').on( 'click', function () {
				$('#loading').show();
				table.columns(0).search($('#myInput').val()).draw();
				setTimeout(function(){$('#loading').hide();}, 3000);
			});
			
			$('#select-choice-mini').on('change',function(){
				table.columns(21).search($(this).val()).draw();
			});
			
			$(window).resize(function() {
				//location.reload();
				$('#loading').show();
				table.draw();
				//var height=getHeight();
				//table.context[0].oScroll.sY=height;
				setTimeout(function(){$('#loading').hide();}, 3000);
			})
			
		
		});
		
		function sleep(d){
			for(var t = Date.now();Date.now() - t <= d;);
		}
		
		function getHeight(){
			if(screen.height>=900){
				return (screen.height*0.75)+'px';
			}else if(screen.height>=600){
				return (screen.height*0.65)+'px';
			}else{
				return (screen.height*0.55)+'px';
			}
		}	
		