$(function () {	
	$('#loading').show();
	$( "#effect" ).hide();
	var isMobileBoolen=isMobile();
	var isMacChromeBoolen=isMacChrome();
	var colWidthS = tdSizeS();//找出寬度
	/*************************設定欄位寬度值************************/
	var colWidthS2 = colWidthS.toString()+"%"; 
	var colWidthS4 = tdSizeS4(colWidthS).toString()+"%";
	var colWidthS6 = tdSizeS6(colWidthS).toString()+"%";
	var colWidthS8 = tdSizeS8(colWidthS).toString()+"%";
	/*************************設定欄位寬度值************************/
	
	var $grid =$("#grid_json");//設定div位置
	var rows;//每頁行數
	if(isMobileBoolen){ //判斷是手機就加入
		rows = 40;
	}else{
		rows = 300;
	}

	var obj = {  
		width:'100%',
		height: '100%',
        numberCell:{show:false},//關掉編號
		filterModel: { mode: 'OR' },
		freezeCols: 2,//固定左邊欄位
		toolbar: {
            cls: "pq-toolbar-search",
            items: [
				{ type: 'textbox', attr: 'size=6 placeholder="代號"', cls: "filterValue", listeners: [{ 'input': filterhandler}] },
				{ type: 'select', id: "select-choice-mini", cls: "filterIndustry",
                    listeners: [{ 'change': filterhandlerIndustry}],
                    options: [
                    { ".0.": "全部類別" },
                    { ".1.": "水泥工業" }, { ".2.": "食品工業" }, { ".3.": "塑膠工業" }, { ".4.": "紡織纖維" },
					{ ".5.": "電機機械" }, { ".6.": "電器電纜" }, { ".7.": "化學工業" }, { ".8.": "生技醫療" },
					{ ".9.": "玻璃陶瓷" }, { ".10.": "造紙工業" }, { ".11.": "鋼鐵工業" }, { ".12.": "橡膠工業" },
					{ ".13.": "汽車工業" }, { ".14.": "半導體業" }, { ".15.": "電腦週邊" }, { ".16.": "光電業" },
					{ ".17.": "通信網路" }, { ".18.": "零組件業" }, { ".19.": "電子通路" }, { ".20.": "資訊服務" },
					{ ".21.": "其他電子" }, { ".22.": "建材營造" }, { ".23.": "航運業" }, { ".24.": "觀光事業" },
					{ ".25.": "金融保險" }, { ".26.": "貿易百貨" }, { ".27.": "油電燃氣" }, { ".28.": "其他" },
					{ ".29.": "存託憑證" }, { ".30.": "受益證券" }, { ".31.": "指數基金" }, { ".32.": "全額交割" }
                    ]
                },
				{ type: 'button', cls:"ui-btn ui-btn-inline",style: 'padding: 0; border: none; background: none; float: right;',
				listener: openMenu,
				label: '<a href=\"#menu\"><img class=\"myMenu\" src=\"./css/images/menu.png\"/></a>', 
				}
            ]
        },
        resizable: false,
		editable: false,
        showBottom: false,
		showTitle: false,
        collapsible: false,
		freezeBorders: false, 
		columnBorders: false, //框線
		rowBorders: false,
		selectionModel: { type: 'row' }, //tr class 
		dragColumns: { enabled: false }
    };
	if(!isMacChromeBoolen){ //如果不是mac+chrome才會加入分頁
		obj.pageModel={ rPP: rows, type: "local"};//分頁
	}
	
    obj.colModel = [
		{ title: "證券代號", width:colWidthS4, dataIndx: "code", dataType: "string" ,sortable: false, render: filterRender},
        { title: "證券名稱", width:colWidthS4, dataIndx: "company", dataType: "string" ,sortable: false, render: filterRender},
        { title: "漲跌", width:colWidthS2, dataIndx: "change", dataType: "float" ,sortable: false},
        { title: "收盤", width:colWidthS2, dataIndx: "close", dataType: "float" ,sortable: false},
	    { title: "次一日", align: "center", colModel: 
		[{ title: "漲停", width:colWidthS2, dataIndx: "nextRiseLimit", dataType: "float" ,sortable: false}, 
		{ title: "跌停", width:colWidthS2, dataIndx: "nextFallLimit", dataType: "float",sortable: false}]},
		{ title: "開盤", width:colWidthS2, dataIndx: "open", dataType: "float" ,sortable: false},
		{ title: "最高", width:colWidthS2, dataIndx: "dayHigh", dataType: "float" ,sortable: false},
		{ title: "最低", width:colWidthS2, dataIndx: "dayLow", dataType: "float" ,sortable: false},
		{ title: "平均每筆張數", width:colWidthS6, dataIndx: "volumePerTrade", dataType: "float" ,sortable: false},
		{ title: "成交數量(千股)", align: "center", colModel: 
		[{ title: "今日", width:colWidthS2, dataIndx: "volume", dataType: "float" ,sortable: false}, 
		{ title: "前一日", width:colWidthS2, dataIndx: "preVolume", dataType: "float",sortable: false}]},
		{ title: "5日RSI",  width:colWidthS4, dataIndx: "RsiPer5", dataType: "float" ,sortable: false},
		{ title: "5日平均量", width:colWidthS6, dataIndx: "volumePer5", dataType: "float" ,sortable: false},
		{ title: "平均值", align: "center", colModel: 
		[{ title: "5日", width:colWidthS2, dataIndx: "pricePer5", dataType: "float" ,sortable: false}, 
		{ title: "20日",  width:colWidthS2, dataIndx: "pricePer20", dataType: "float",sortable: false}]},
		{ title: "10日乖離值",  width:colWidthS6, dataIndx: "BiasPer10", dataType: "float" ,sortable: false},
		{ title: "去年來", align: "center", colModel: 
		[{ title: "最高", width:colWidthS2, dataIndx: "lastYearHigh", dataType: "float" ,sortable: false}, 
		{ title: "最低",  width:colWidthS2, dataIndx: "lastYearLow", dataType: "float",sortable: false}]},
		{ title: "交易所本益比",  width:colWidthS6, dataIndx: "PE", dataType: "float" ,sortable: false},
		{ title: "期末股本(百萬元)", width:colWidthS8, dataIndx: "capital", dataType: "float" ,sortable: false},
		{ title: "類別", dataIndx: "industry", dataType: "string" ,sortable: false, hidden:true}
    ];
	
    obj.dataModel = { data: data };//放入資料
	$grid.pqGrid(obj);//產生table
	if(!isMacChromeBoolen){ //如果不是mac+chrome才會加入分頁相關功能
		hidePage();//隱藏分頁欄位
		var preScrollValue=0;
		$grid.on('swipeup', function(e) { //偵測手指往上方向來決定是否換頁
		var scrollValue=$(".pq-grid-cont-inner").scrollTop();
		if(scrollValue==preScrollValue){ //滾輪確定到最底層
			nextPage();//下一頁
			preScrollValue=0;//回復預設
		}else{
			preScrollValue=scrollValue;//覆蓋
		} 
		});
	
		$grid.on('swipedown', function(e) { //偵測手指往下方向來決定是否換頁
		var scrollValue=$(".pq-grid-cont-inner").scrollTop();
		if(scrollValue == 0){ //滾輪最上層
			previous();	//上一頁
		}  
		});
	
		$grid.bind('mousewheel DOMMouseScroll', function(event){ //偵側滑鼠滾輪來決定是否換頁
		if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
			previous();	
		}else {
			nextPage();
		}
		});	
	}

	if(isMobileBoolen){ //判斷是手機就加入
		setTimeout(function(){$('#loading').hide();}, 3000);
	}else{
		$grid.pqGrid( "option", "virtualY", true );
		setTimeout(function(){$('#loading').hide();}, 5000);
	}

	
	$('nav#menu').mmenu({//啟動 menu plugin
		extensions: ["tileview","pagedim-black","theme-dark"],
		offCanvas: { position: "right" },
		navbars  : [
			{
				position : 'top',
				content  : ['close','title']
			}
		],
		'slidingSubmenus': false
	});	
	var menuAPI = $('nav#menu').data("mmenu");
	
	$("#stockDialog").dialog({  //上市摘要
	width: 'auto',
    height: 'auto',
		show: { effect: "slide"},
        autoOpen: false,   
        modal: true,  
        close: function() {   
            $("#stockDialog").dialog("close");  
        }   
	});  
	$("#stockAbstract").click(function() { //上市摘要
		$( "#stockDialog").dialog("open");   
	});
	
	$("#otcDialog").dialog({  //上櫃摘要
		width: 'auto',
		height: 'auto',
		show: { effect: "slide"},
        autoOpen: false,   
        modal: true,  
        close: function() {   
            $("#otcDialog").dialog("close");  
        }   
	});  
	$("#otcAbstract").click(function() { //上櫃摘要
		$( "#otcDialog").dialog("open");   
	});
	
	
	$(window).resize(function() {
		menuAPI.close();
		var colWidthS = tdSizeS();
		var colWidthS2 = colWidthS.toString()+"%";
		var colWidthS4 = tdSizeS4(colWidthS).toString()+"%";
		var colWidthS6 = tdSizeS6(colWidthS).toString()+"%";
		var colWidthS8 = tdSizeS8(colWidthS).toString()+"%";
		var colM=$grid.pqGrid( "option", "colModel" );
		colM[0].width = colWidthS4;
        colM[1].width = colWidthS4;
        colM[2].width = colWidthS2;colM[3].width = colWidthS2;
		colM[4].colModel[0].width = colWidthS2;colM[4].colModel[1].width = colWidthS2;
		colM[5].width = colWidthS2;colM[6].width = colWidthS2;colM[7].width = colWidthS2;
		colM[8].width = colWidthS6;
		colM[9].colModel[0].width = colWidthS2;colM[9].colModel[1].width = colWidthS2;
		colM[10].width = colWidthS4;
		colM[11].width = colWidthS6;
		colM[12].colModel[0].width = colWidthS2;colM[12].colModel[1].width = colWidthS2;
		colM[13].width = colWidthS6;
		colM[14].colModel[0].width = colWidthS2;colM[14].colModel[1].width = colWidthS2;
		colM[15].width = colWidthS6;
		colM[16].width = colWidthS8;
		$("#grid_json").pqGrid("option","colModel", colM );
		$("#grid_json").pqGrid("option", { width: getBrowserWidth(),height: getBrowserHeight() });  
		$("#grid_json").pqGrid('refresh');
	});
	
	function openMenu(){
		menuAPI.open();		
	}
	
	function filterhandler(evt, ui) {
        var $toolbar = $grid.find('.pq-toolbar-search'),
            $value = $toolbar.find(".filterValue"),
            value = $value.val(),
            condition = "begin",
            filterObject= [];
			filterObject.push({ dataIndx: "code", condition: condition, value: value });
			//filterObject.push({ dataIndx: "company", condition: condition, value: value});	
        $grid.pqGrid("filter", {
            oper: 'replace', data: filterObject
        });
		$grid.pqGrid( "goToPage", { page: 1} ); //回到第一頁
		$grid.pqGrid( "scrollRow", { rowIndxPage: 0 } ); //滾輪到最上層
    }
	
	function filterhandlerIndustry(evt, ui) {
        var $toolbar = $grid.find('.pq-toolbar-search'),
			industry = $toolbar.find(".filterIndustry").val(),
            filterObject = [];
		//判斷類別(industry)
		if((industry != ".0.") && (typeof(industry) != 'undefined')){
			filterObject.push({ dataIndx: 'industry', condition: "equal", value: industry});
		} else {
			var CM = $grid.pqGrid("getColModel");
            for (var i = 0, len = CM.length; i < len; i++) {
                var dataIndx = CM[i].dataIndx;
                filterObject.push({ dataIndx: dataIndx, condition: "equal", value: "" });
            }
		}	
        $grid.pqGrid("filter", {
            oper: 'replace', data: filterObject
        });
		$grid.pqGrid( "goToPage", { page: 1} ); //回到第一頁
		$grid.pqGrid( "scrollRow", { rowIndxPage: 0 } ); //滾輪到最上層
    }
	
	function filterRender(ui) {
        var val = ui.cellData,
                filter = ui.column.filter;
        if (filter && filter.on && filter.value) {
            var condition = filter.condition,
                valUpper = val.toUpperCase(),
                txt = filter.value,
                txt = (txt == null) ? "" : txt.toString(),
                txtUpper = txt.toUpperCase(),
                indx = -1;
            if (condition == "end") {
                indx = valUpper.lastIndexOf(txtUpper);
                if (indx + txtUpper.length != valUpper.length) {
                    indx = -1;
                }
            } else if (condition == "contain") {
                indx = valUpper.indexOf(txtUpper);
            } else if (condition == "begin") {
                indx = valUpper.indexOf(txtUpper);
                if (indx > 0) {
                    indx = -1;
                }
            }
            if (indx >= 0) {
                var txt1 = val.substring(0, indx);
                var txt2 = val.substring(indx, indx + txt.length);
                var txt3 = val.substring(indx + txt.length);
                return txt1 + "<span style='background:yellow;color:#333;'>" + txt2 + "</span>" + txt3;
            } else {
                return val;
            }
        } else {
            return val;
        }
    }
	
	function hidePage(){ //隱藏分頁
		$("div").css('borderWidth', '0px');
		$("#grid_json").find(".pq-header-outer").css('borderWidth', '1px');
		$("#grid_json").find(".pq-grid-cont-outer").css('borderWidth', '1px');
		$("#grid_json").find(".pq-pager").hide();
		$("#grid_json").pqGrid("refresh");
	}
	
	function nextPage(){ //下一頁
		var finalPage = parseInt($(".total")[0].textContent)+1;//最後一頁的判斷
		var pageN = parseInt($(".pq-pager-input").val())+1;
		$("#grid_json").pqGrid( "goToPage", { page: pageN} );
		if(pageN!=finalPage){//最後一頁的判斷
			$("#grid_json").pqGrid( "scrollRow", { rowIndxPage: 0 } );
		}
	}
	
	function previous(){ //上一頁
		var pageP=parseInt($(".pq-pager-input").val())-1;
		if(pageP!=0){ //第一頁的判斷
			$("#grid_json").pqGrid( "goToPage", { page: pageP} );
			$("#grid_json").pqGrid( "scrollRow", { rowIndxPage: (rows-1) } ); 
		}
	}
});
