﻿namespace Transformer
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.Btn_ProcessByByte = new System.Windows.Forms.Button();
            this.Btn_ProcessByRegex = new System.Windows.Forms.Button();
            this.ProcessInfo = new System.Windows.Forms.ListView();
            this.SelectList = new System.Windows.Forms.CheckedListBox();
            this.SuspendLayout();
            // 
            // Btn_ProcessByByte
            // 
            this.Btn_ProcessByByte.Location = new System.Drawing.Point(12, 12);
            this.Btn_ProcessByByte.Name = "Btn_ProcessByByte";
            this.Btn_ProcessByByte.Size = new System.Drawing.Size(81, 41);
            this.Btn_ProcessByByte.TabIndex = 0;
            this.Btn_ProcessByByte.Text = "以Byte處理";
            this.Btn_ProcessByByte.UseVisualStyleBackColor = true;
            this.Btn_ProcessByByte.Click += new System.EventHandler(this.Btn_ProcessByRByte_Click);
            // 
            // Btn_ProcessByRegex
            // 
            this.Btn_ProcessByRegex.Location = new System.Drawing.Point(99, 12);
            this.Btn_ProcessByRegex.Name = "Btn_ProcessByRegex";
            this.Btn_ProcessByRegex.Size = new System.Drawing.Size(82, 41);
            this.Btn_ProcessByRegex.TabIndex = 1;
            this.Btn_ProcessByRegex.Text = "以Regex處理";
            this.Btn_ProcessByRegex.UseVisualStyleBackColor = true;
            this.Btn_ProcessByRegex.Click += new System.EventHandler(this.Btn_ProcessByRegex_Click);
            // 
            // ProcessInfo
            // 
            this.ProcessInfo.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProcessInfo.Location = new System.Drawing.Point(12, 73);
            this.ProcessInfo.Name = "ProcessInfo";
            this.ProcessInfo.Size = new System.Drawing.Size(293, 130);
            this.ProcessInfo.TabIndex = 2;
            this.ProcessInfo.UseCompatibleStateImageBehavior = false;
            this.ProcessInfo.View = System.Windows.Forms.View.List;
            // 
            // SelectList
            // 
            this.SelectList.CheckOnClick = true;
            this.SelectList.FormattingEnabled = true;
            this.SelectList.Items.AddRange(new object[] {
            "日上市",
            "日上櫃",
            "日興櫃"});
            this.SelectList.Location = new System.Drawing.Point(187, 12);
            this.SelectList.Name = "SelectList";
            this.SelectList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.SelectList.Size = new System.Drawing.Size(118, 55);
            this.SelectList.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(317, 215);
            this.Controls.Add(this.SelectList);
            this.Controls.Add(this.ProcessInfo);
            this.Controls.Add(this.Btn_ProcessByRegex);
            this.Controls.Add(this.Btn_ProcessByByte);
            this.Name = "Form1";
            this.Text = "聯晚行情表Prn轉html";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Btn_ProcessByByte;
        private System.Windows.Forms.Button Btn_ProcessByRegex;
        private System.Windows.Forms.ListView ProcessInfo;
        private System.Windows.Forms.CheckedListBox SelectList;
    }
}

