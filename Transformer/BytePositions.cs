﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tranfromer
{
    public static class bytePos
    {
        //{起始位置, 所取長度}
        public static List<int> Industry = new List<int>() { 0, 1 };
        public static List<int> Code = new List<int>() { 1, 6 };
        public static List<int> Company = new List<int>() { 7, 7 };
        public static List<int> Change = new List<int>() { 14, 7 };
        public static List<int> Close = new List<int>() { 21, 8 };
        public static List<int> NextRiseLimit = new List<int>() { 29, 6 };
        public static List<int> NextFallLimit = new List<int>() { 35, 6 };
        public static List<int> Open = new List<int>() { 41, 8 };
        public static List<int> DayHigh = new List<int>() { 49, 8 };
        public static List<int> DayLow = new List<int>() { 57, 8 };
        public static List<int> VolumePerTrade = new List<int>() { 65, 6 };
        public static List<int> Volume = new List<int>() { 71, 7 };
        public static List<int> PreVolume = new List<int>() { 78, 7 };
        public static List<int> RsiPer5 = new List<int>() { 85, 6 };
        public static List<int> VolumePer5 = new List<int>() { 91, 6 };
        public static List<int> PricePer5 = new List<int>() { 97, 6 };
        public static List<int> PricePer20 = new List<int>() { 103, 6 };
        public static List<int> BiasPer10 = new List<int>() { 109, 6 };
        public static List<int> LastYearHigh = new List<int>() { 115, 6 };
        public static List<int> LastYearLow = new List<int>() { 121, 6 };
        public static List<int> PE = new List<int>() { 127, 7 };
        public static List<int> Capital = new List<int>() { 134, 6 };
    };
}
