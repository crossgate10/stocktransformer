﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Transformer.JsonModel
{
    public class JsonType1
    {
        private string _industry;
        [ScriptIgnore]
        public string industry
        {
            get
            {
                if (_industry != "") return _industry;
                else return null;
            }
            set
            {
                _industry = value;
            }
        }
        public string code { get; set; }
        public string company { get; set; }
        private string _symbolClose;
        [ScriptIgnore]
        public string symbolClose {
            get {
                if (_symbolClose != "") return _symbolClose;
                else return null;
            }
            set {
                _symbolClose = value;
            }
        }
        public string change { get; set; }
        public string close { get; set; }
        public string nextRiseLimit { get; set; }
        public string nextFallLimit { get; set; }
        private string _symbolOpen;
        [ScriptIgnore]
        public string symbolOpen
        {
            get
            {
                if (_symbolOpen != "") return _symbolOpen;
                else return null;
            }
            set
            {
                _symbolOpen = value;
            }
        }
        public string open { get; set; }
        private string _symbolDayHigh;
        [ScriptIgnore]
        public string symbolDayHigh
        {
            get
            {
                if (_symbolDayHigh != "") return _symbolDayHigh;
                else return null;
            }
            set
            {
                _symbolDayHigh = value;
            }
        }
        public string dayHigh { get; set; }
        private string _symbolDayLow;
        [ScriptIgnore]
        public string symbolDayLow
        {
            get
            {
                if (_symbolDayLow != "") return _symbolDayLow;
                else return null;
            }
            set
            {
                _symbolDayLow = value;
            }
        }
        public string dayLow { get; set; }
        public string volumePerTrade { get; set; }
        public string volume { get; set; }
        public string preVolume { get; set; }
        public string RsiPer5 { get; set; }
        public string volumePer5 { get; set; }
        public string pricePer5 { get; set; }
        public string pricePer20 { get; set; }
        public string BiasPer10 { get; set; }
        public string lastYearHigh { get; set; }
        public string lastYearLow { get; set; }
        public string PE { get; set; }
        public string capital { get; set; }
    }
}
