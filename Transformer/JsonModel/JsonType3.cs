﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transformer.JsonModel
{
    public class JsonType3
    {
        public string company { get; set; }
        public string preAveragePrice { get; set; }
        public string buyingPrice { get; set; }
        public string sellingPrice { get; set; }
    }
}
