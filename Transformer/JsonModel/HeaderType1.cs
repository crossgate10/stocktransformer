﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transformer.JsonModel
{
    public class HeaderType1
    {
        public string closingIndex { get; set; }
        public string nonFinance { get; set; }
        public string nonElectronics { get; set; }
        public string nonFinanceElectronics { get; set; }
        public string dealAmount { get; set; }
        public string DayHighIndex { get; set; }
        public string DayLowIndex { get; set; }
        public string TW50Index { get; set; }
        public string formosaIndex { get; set; }
        public string salaryHigh100Index { get; set; }
        public string electronicsRewardIndex { get; set; }
        public string financeRewardIndex { get; set; }
        public string cementAndCeramic { get; set; }
        public string plasticAndChemical { get; set; }
        public string electrical { get; set; }
        public string cement { get; set; }
        public string food { get; set; }
        public string plastic { get; set; }
        public string textile { get; set; }
        public string electricMachinery { get; set; }
        public string electricalAndCable { get; set; }
        public string chemicalBiotechnologyAndMedicalCare { get; set; }
        public string glassAndCeramic { get; set; }
        public string paperAndPulp { get; set; }
        public string ironAndSteel { get; set; }
        public string rubber { get; set; }
        public string automobile { get; set; }
        public string electronics { get; set; }
        public string buildingMaterialAndConstruction { get; set; }
        public string shippingAndTransportation { get; set; }
        public string tourism { get; set; }
        public string financeAndInsurance { get; set; }
        public string tradingAndConsumersGoods { get; set; }
        public string other { get; set; }
        public string chemical { get; set; }
        public string biotechnologyAndMedicalCare { get; set; }
        public string semiconductor { get; set; }
        public string computerAndPeripheralEquipment { get; set; }
        public string optoelectronic { get; set; }
        public string communicationsAndInternet { get; set; }
        public string electronicPartsComponents { get; set; }
        public string electronicProductsDistribution { get; set; }
        public string informationService { get; set; }
        public string otherElectronic { get; set; }
        public string oilGasAndElectricity { get; set; }
    }
}
