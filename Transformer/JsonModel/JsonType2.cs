﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transformer.JsonModel
{
    public class JsonType2
    {
        public string industry { get; set; }
        public string code { get; set; }
        public string company { get; set; }
        public string symbolClose { get; set; }
        public string change { get; set; }
        public string close { get; set; }
        public string nextRiseLimit { get; set; }
        public string nextFallLimit { get; set; }
        public string symbolOpen { get; set; }
        public string open { get; set; }
        public string symbolDayHigh { get; set; }
        public string dayHigh { get; set; }
        public string symbolDayLow { get; set; }
        public string dayLow { get; set; }
        public string volume { get; set; }
        public string capital { get; set; }
        public string PE { get; set; }

    }
}
