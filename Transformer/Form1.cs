﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Transformer.JsonModel;

namespace Transformer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        #region ByteReader版
        public static class bytePos
        {
            public static int[] Industry = { 1, 1 };
            public static int[] Code = { };
            public static int[] Company = { };
            public static int[] Change = { };
            public static int[] Close = { };
            public static int[] NextRiseLimit = { };
            public static int[] NextFallLimit = { };
            public static int[] Open = { };
            public static int[] DayHigh = { };
            public static int[] DayLow = { };
            public static int[] VolumePerTrade = { };
            public static int[] Volume = { };
            public static int[] PreVolume = { };
            public static int[] RsiPer5 = { };
            public static int[] VolumePer5 = { };
            public static int[] PricePer5 = { };
            public static int[] PricePer20 = { };
            public static int[] BiasPer10 = { };
            public static int[] LastYearHigh = { };
            public static int[] LastYearLow = { };
            public static int[] PE = { };
            public static int[] Capital = { };
        }

        private void Btn_ProcessByRByte_Click(object sender, EventArgs e)
        {
            var dataSet = LoadPrnToString();
            GenerateHTMLTable(dataSet);
        }

        private string[] LoadPrnToString()
        {
            string[] strArray = File.ReadLines(AppDomain.CurrentDomain.BaseDirectory + "\\Data\\Nstock4.prn", Encoding.Default).ToArray();
            List<string> result = new List<string>();
            string industry, code, company, change, close, nextRiseLimit, nextFallLimit, open, dayHigh, dayLow,
                    volumePerTrade, volume, preVolume, rsiPer5, volumePer5, pricePer5, pricePer20, biasPer10,
                    lastYearHigh, lastYearLow, pe, capital;
            string tmp = "";

            foreach (var str in strArray)
            {
                tmp = "";
                if (str == "A") { break; }

                //str to byte
                byte[] byteArr = Encoding.Default.GetBytes(str);

                industry = Encoding.Default.GetString(byteArr, bytePos.Industry[0], bytePos.Industry[1]);
                code = Encoding.Default.GetString(byteArr, bytePos.Code[0], bytePos.Code[1]);
                company = Encoding.Default.GetString(byteArr, bytePos.Company[0], bytePos.Company[1]);
                change = Encoding.Default.GetString(byteArr, bytePos.Change[0], bytePos.Change[1]);
                close = Encoding.Default.GetString(byteArr, bytePos.Close[0], bytePos.Close[1]);
                nextRiseLimit = Encoding.Default.GetString(byteArr, bytePos.NextRiseLimit[0], bytePos.NextRiseLimit[1]);
                nextFallLimit = Encoding.Default.GetString(byteArr, bytePos.NextFallLimit[0], bytePos.NextFallLimit[1]);
                open = Encoding.Default.GetString(byteArr, bytePos.Open[0], bytePos.Open[1]);
                dayHigh = Encoding.Default.GetString(byteArr, bytePos.DayHigh[0], bytePos.DayHigh[1]);
                dayLow = Encoding.Default.GetString(byteArr, bytePos.DayLow[0], bytePos.DayLow[1]);
                volumePerTrade = Encoding.Default.GetString(byteArr, bytePos.VolumePerTrade[0], bytePos.VolumePerTrade[1]);
                volume = Encoding.Default.GetString(byteArr, bytePos.Volume[0], bytePos.Volume[1]);
                preVolume = Encoding.Default.GetString(byteArr, bytePos.PreVolume[0], bytePos.PreVolume[1]);
                rsiPer5 = Encoding.Default.GetString(byteArr, bytePos.RsiPer5[0], bytePos.RsiPer5[1]);
                volumePer5 = Encoding.Default.GetString(byteArr, bytePos.VolumePer5[0], bytePos.VolumePer5[1]);
                pricePer5 = Encoding.Default.GetString(byteArr, bytePos.PricePer5[0], bytePos.PricePer5[1]);
                pricePer20 = Encoding.Default.GetString(byteArr, bytePos.PricePer20[0], bytePos.PricePer20[1]);
                biasPer10 = Encoding.Default.GetString(byteArr, bytePos.BiasPer10[0], bytePos.BiasPer10[1]);
                lastYearHigh = Encoding.Default.GetString(byteArr, bytePos.LastYearHigh[0], bytePos.LastYearHigh[1]);
                lastYearLow = Encoding.Default.GetString(byteArr, bytePos.LastYearLow[0], bytePos.LastYearLow[1]);
                pe = Encoding.Default.GetString(byteArr, bytePos.PE[0], bytePos.PE[1]);
                capital = Encoding.Default.GetString(byteArr, bytePos.Capital[0], bytePos.Capital[1]);

                tmp = industry + "," + code + "," + company + "," + change + "," + close + "," + nextRiseLimit
                                + "," + nextFallLimit + "," + open + "," + dayHigh + "," + dayLow + "," + volumePerTrade
                                + "," + volume + "," + preVolume + "," + rsiPer5 + "," + volumePer5 + "," + pricePer5
                                + "," + pricePer20 + "," + biasPer10 + "," + lastYearHigh + "," + lastYearLow + "," + pe + "," + capital;
                result.Add(tmp);
            }

            return result.ToArray();
        }

        private void GenerateHTMLTable(string[] dataSet)
        {
            //string header = "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\">";
            //string footer = "</table>";
            string header = "<tbody>\n";
            string footer = "</tbody>";
            string content = "";

            int count = 0;
            string[] dataArr;
            foreach (var data in dataSet)
            {
                dataArr = data.Split(',');
                content += "<tr>\n";
                count = 0;
                foreach (var s in dataArr)
                {
                    if (count != 0)
                    {
                        if (count == 1)
                            content += "<th>" + s;
                        else if (count == 2)
                            content += " " + s + "</th>\n";
                        else
                            content += "<td>" + s + "</td>\n";
                    }
                    count++;
                }
                content += "</tr>\n";
            }
            System.IO.File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + @"\\table.html", header + content + footer);
            ProcessInfo.Items.Add("HTML 產生完成");
        }
        #endregion

        #region Regex版
        private void Btn_ProcessByRegex_Click(object sender, EventArgs e)
        {
            List<string> patterns = new List<string>();
            List<List<int>> capturedIndexesList = new List<List<int>>();
            String pattern1 = "([\\s1])(\\d{4}..)(.{3,6})([X^\\s])" +
                "(([\\d\\s]{3}\\d\\.\\d{2})|(\\s{5}-\\s))([\\+\\-\\s])(.{7})(.{6})(.{6})([\\+\\-\\s])(.{7})([\\+\\-\\s])(.{7})([\\+\\-\\s])(.{7})(.{6})(.{7})(.{7})" +
                "(.{6})(.{6})(.{6})(.{6})(.{6})(.{6})(.{6})(.{7})" +
                "(([\\d\\s]{6})|(\\s{4}-\\s))";
            List<int> capturedIndexes1 = new List<int>() { 1, 2, 3, 4, 5, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29 };

            String pattern2 = "(\\d{4}..)(.{2,6})(\\s{10})(.{6})(.{6})(.{6})(.{6})" +
                "([\\+\\-\\^\\svX])(.{6})(.{12})(.{6})(.{6})(.{24})(.{9})(.{22})(.{9})(.{19})(.{6})(.{2})";
            List<int> capturedIndexes2 = new List<int>() { 1, 2, 4, 5, 6, 7, 8, 9, 11, 12, 14, 16, 18, 19 };

            patterns.Add(pattern1);
            patterns.Add(pattern2);
            capturedIndexesList.Add(capturedIndexes1);
            capturedIndexesList.Add(capturedIndexes2);
            ProcessByRegex(patterns, capturedIndexesList, SelectList.CheckedIndices.OfType<int>().ToList());
        }

        private void ProcessByRegex(List<string> patterns, List<List<int>> capturedIndexesList, List<int> choices)
        {
            Encoding enc = Encoding.GetEncoding(950);   // Big-5
            int errorNo = 0;
            List<string> filePath = new List<string>() 
            {
                //每日上市來源檔案
                AppDomain.CurrentDomain.BaseDirectory + @"\\Data\\Nstock0.prn",
                AppDomain.CurrentDomain.BaseDirectory + @"\\Data\\Nstock4.prn",
                //每日上櫃來源檔案
                AppDomain.CurrentDomain.BaseDirectory + @"\\Data\\STKSECTINX.TXT",
                AppDomain.CurrentDomain.BaseDirectory + @"\\Data\\STKT1QUOTESN.TXT",
                AppDomain.CurrentDomain.BaseDirectory + @"\\Data\\STKT1SUMMARY.TXT",
                //每日興櫃來源檔案
                AppDomain.CurrentDomain.BaseDirectory + @"\\Data\\new.csv"
            };
            List<string> result;
            string tmp;

            foreach (var choice in choices)
            {
                //Type 3 normalize and split the input line
                if (choice == 2)
                {
                    using (StreamReader sr = new StreamReader(filePath[choice + 3], enc))
                    {
                        result = splitCSV(sr.ReadToEnd()).ToList<string>();
                    }
                }
                else
                {
                    Regex rx = new Regex(patterns[choice], RegexOptions.Compiled);
                    tmp = "";
                    result = new List<string>();
                    try
                    {   // Open the text file using a stream reader.
                        using (StreamReader sr = new StreamReader(filePath[choice+2], enc))
                        {
                            // Read the stream to a string, and write the string to the console.
                            while (sr.Peek() >= 0)
                            {
                                String line = sr.ReadLine();
                                Match match = rx.Match(line);
                                if (match.Success)
                                {
                                    foreach (String s in capturedIndexesList[choice].Select<int, String>(i => (match.Groups[i].Value)))
                                    {
                                        tmp += s.Trim() + ",";
                                    }
                                    result.Add(tmp);
                                    tmp = "";
                                }
                                else
                                {
                                    //ProcessInfo.Items.Add("Error: " + line);
                                    errorNo++;
                                    // TODO: Log error
                                }
                            }
                        }
                        //ProcessInfo.Items.Add("error No: " + errorNo);
                    }
                    catch (Exception ex)
                    {
                        //ProcessInfo.Items.Add("The file could not be read:" + ex.Message);
                    }
                }

                switch (choice)
                {
                    case 0:
                        GenJsonType1ByRegex(result.ToArray()); //上市
                        break;
                    case 1:
                        GenJsonType2ByRegex(result.ToArray()); //上櫃
                        break;
                    case 2:
                        GenJsonType3ByRegex(result.ToArray()); //興櫃
                        break;
                }
                //GenHTMLTableByRegx(result.ToArray());
                //GenJsonByRegx(result.ToArray());
            }
        }

        private string[] splitCSV(string p)
        {
            var s = p.Replace("\"", "|");
            return s.Split(new string[] { "\r\n" }, StringSplitOptions.None);
        }

        private void GenHTMLTableByRegx(string[] dataSet)
        {
            string header = "<table cellpadding=\"0\" cellspacing=\"0\" border=\"1\">";
            string footer = "</table>";
            //string header = "<tbody>\n";
            //string footer = "</tbody>";
            string content = "";

            int count = 0;
            string[] dataArr;
            foreach (var data in dataSet)
            {
                dataArr = data.Split(',');
                content += "<tr>\n";
                count = 0;
                foreach (var s in dataArr)
                {
                    if (count != 0)
                    {
                        if (count == 1)
                            content += "<td align=\"right\">" + s;
                        else if (count == 2)
                            content += " " + s + "</td>\n";
                        else
                            content += "<td align=\"right\">" + s + "</td>\n";
                    }
                    count++;
                }
                content += "</tr>\n";
            }
            System.IO.File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + @"\\table.html", header + content + footer);
            ProcessInfo.Items.Add("HTML 產生完成");
        }

        private void GenJsonByRegex(string[] dataSet)
        {
            string[] dataArr;
            List<object> result = new List<object>();
            foreach (var data in dataSet)
            {
                dataArr = data.Split(',');
                result.Add(new
                {
                    code = dataArr[1],
                    name = dataArr[2],
                    change = dataArr[3],
                    closing = dataArr[4],
                    nextRiseLimit = dataArr[5],
                    nextFallLimit = dataArr[6],
                    open = dataArr[7],
                    dayHigh = dataArr[8],
                    dayLow = dataArr[9],
                    volumePerTrade = dataArr[10],
                    volume = dataArr[11],
                    preVolume = dataArr[12],
                    RsiPer5 = dataArr[13],
                    volumePer5 = dataArr[14],
                    pricePer5 = dataArr[15],
                    pricePer20 = dataArr[16],
                    BiasPer10 = dataArr[17],
                    lastYearHigh = dataArr[18],
                    laseYearLow = dataArr[19],
                    PE = dataArr[20],
                    capital = dataArr[21]
                });
            }
            var tmp = new
            {
                data = result.ToArray()
            };
            string json = JsonConvert.SerializeObject(tmp);
            System.IO.File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + @"\\table.json", json.ToString());
            ProcessInfo.Items.Add("Json 產生完成");
        }

        private void GenJsonType1ByRegex(string[] dataSet)
        {
            string[] dataArr;
            List<object> result = new List<object>();
            JsonType1 obj;
            int industryCount = 0;
            foreach (var data in dataSet)
            {
                dataArr = data.Split(',');
                obj = new JsonType1();
                //obj.industry = dataArr[0];
                if (dataArr[0] == "1") { industryCount++; }
                obj.industry = industryCount.ToString();
                obj.code = dataArr[1];
                obj.company = dataArr[2];
                if (dataArr[9] == "-") { obj.symbolClose = "-"; }
                else if (dataArr[9] == "+") { obj.symbolClose = "+"; }
                else { obj.symbolClose = dataArr[3]; }
                obj.change = dataArr[4];
                obj.close = dataArr[10];
                obj.nextRiseLimit = dataArr[7];
                obj.nextFallLimit = dataArr[8];
                obj.symbolOpen = dataArr[5];
                obj.open = dataArr[6];
                obj.symbolDayHigh = dataArr[11];
                obj.dayHigh = dataArr[12];
                obj.symbolDayLow = dataArr[13];
                obj.dayLow = dataArr[14];
                obj.volumePerTrade = dataArr[15];
                obj.volume = dataArr[16];
                obj.preVolume = dataArr[17];
                obj.RsiPer5 = dataArr[18];
                obj.volumePer5 = dataArr[19];
                obj.pricePer5 = dataArr[20];
                obj.pricePer20 = dataArr[21];
                obj.BiasPer10 = dataArr[22];
                obj.lastYearHigh = dataArr[23];
                obj.lastYearLow = dataArr[24];
                obj.PE = dataArr[25];
                obj.capital = dataArr[26];
                result.Add(obj);
            }
            int len = result.ToArray().Length;
            var tmp = new
            {
                draw = 1,
                recordsTotal = len,
                recordsFiltered = len,
                data = result.ToArray()
            };
            JsonSerializerSettings setting = new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore
            };
            string json = JsonConvert.SerializeObject(tmp, setting);
            System.IO.File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + @"\\contentTable1.json", json.ToString());
            ProcessInfo.Items.Add("每日上市內容 Json 產生完成");
        }

        private void GenJsonType2ByRegex(string[] dataSet)
        {
            List<object> result = new List<object>();
            string[] dataArr;
            JsonType2 obj;

            foreach (var data in dataSet.Skip(1))
            {
                dataArr = data.Split(','); 
                if(dataArr[0].Length > 4 && dataArr[0] != "006201"){
                    break;
                }
                obj = new JsonType2();
                obj.code = dataArr[0];
                obj.company = dataArr[1];
                obj.open = (Convert.ToDouble(dataArr[2]) / 100).ToString("F2");
                obj.dayHigh = (Convert.ToDouble(dataArr[3]) / 100).ToString("F2");
                obj.dayLow = (Convert.ToDouble(dataArr[4]) / 100).ToString("F2");
                obj.close = (Convert.ToDouble(dataArr[5]) / 100).ToString("F2");
                obj.symbolClose = dataArr[6];
                obj.change = (Convert.ToDouble(dataArr[7]) / 100).ToString("F2");
                obj.nextRiseLimit = (Convert.ToDouble(dataArr[8]) / 100).ToString("F2");
                obj.nextFallLimit = (Convert.ToDouble(dataArr[9]) / 100).ToString("F2");
                obj.volume = (Convert.ToInt32(dataArr[10])).ToString();
                obj.capital = (Convert.ToInt32(dataArr[11])).ToString();
                try
                {
                    obj.PE = (Convert.ToDouble(dataArr[12]) / 100).ToString("F2");
                }
                catch (FormatException)
                {
                    obj.PE = dataArr[12];
                }
                obj.industry = dataArr[13];
                result.Add(obj);
            }

            int len = result.ToArray().Length;
            var tmp = new
            {
                draw = 1,
                recordsTotal = len,
                recordsFiltered = len,
                data = result.ToArray()
            };
            JsonSerializerSettings setting = new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore
            };
            string json = JsonConvert.SerializeObject(tmp, setting);
            System.IO.File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + @"\\table2.json", json.ToString());
            ProcessInfo.Items.Add("每日上櫃 Json 產生完成");
        }

        private void GenJsonType3ByRegex(string[] dataSet)
        {
            List<object> result = new List<object>();
            string[] dataArr;
            JsonType3 obj;
            string[] subDataSet = dataSet.Skip(3).Take(dataSet.Length-7).ToArray();
            foreach (var data in subDataSet)
            {
                dataArr = data.Split(new string[] { "|,|" }, StringSplitOptions.None);
                obj = new JsonType3();
                obj.company = dataArr[1];
                obj.preAveragePrice = dataArr[2];
                obj.buyingPrice = dataArr[3];
                obj.sellingPrice = dataArr[5];
                result.Add(obj);
            }
            int len = result.ToArray().Length;
            var tmp = new
            {
                draw = 1,
                recordsTotal = len,
                recordsFiltered = len,
                data = result.ToArray()
            };
            JsonSerializerSettings setting = new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore
            };
            string json = JsonConvert.SerializeObject(tmp, setting);
            System.IO.File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + @"\\contentTable3.json", json.ToString());
            ProcessInfo.Items.Add("每日興櫃內容 Json 產生完成");
        }

        #endregion
    }
}
