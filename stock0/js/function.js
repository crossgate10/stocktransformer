function getBrowserHeight() { //取得瀏覽器視窗高度
    if ($.browser) {
        return document.compatMode == "CSS1Compat" ? document.documentElement.clientHeight :
                 document.body.clientHeight;
    } else {
        return self.innerHeight;
    }
}

function getBrowserWidth() { //取得瀏覽器視窗寬度
    if ($.browser) {
        return document.compatMode == "CSS1Compat" ? document.documentElement.clientWidth :
                 document.body.clientWidth;
    } else {
        return self.innerWidth;
    }
} 

var size=[10,12,14,16,18,20]; //設定各種螢幕大小的table寬度

function tdSizeS(){ //取得table寬度值
	var BrowserWidth = getBrowserWidth(); //alert(BrowserWidth);
	if(BrowserWidth >= 1200){
		return size[0];
	}else if(BrowserWidth >= 980 && BrowserWidth <= 1199){
		return size[1];
	}else if(BrowserWidth >= 768 && BrowserWidth <= 979){
		return size[2];
	}else if(BrowserWidth >= 481 && BrowserWidth <= 767){
		return size[3];
	}else if(BrowserWidth >= 361 && BrowserWidth <= 480){
		return size[4];
	}else if(BrowserWidth <= 360){
		return size[5];
	}
}

function tdSizeS4(n){ //取得table寬度值
	switch(n)
	{
	case size[0]:{return n+2;break;}
	case size[1]:{return n+2;break;}
	case size[2]:{return n;break;}
	case size[3]:{return n;break;}
	case size[4]:{return n+2;break;}
	case size[5]:{return n+2;break;}
	}
}

function tdSizeS6(n){ //取得table寬度值
	switch(n)
	{
	case size[0]:{return n+6;break;}
	case size[1]:{return n+6;break;}
	case size[2]:{return n+6;break;}
	case size[3]:{return n+10;break;}
	case size[4]:{return n+10;break;}
	case size[5]:{return n+10;break;}
	}
}

function tdSizeS8(n){ //取得table寬度值
	switch(n)
	{
	case size[0]:{return n+10;break;}
	case size[1]:{return n+10;break;}
	case size[2]:{return n+12;break;}
	case size[3]:{return n+16;break;}
	case size[4]:{return n+18;break;}
	case size[5]:{return n+20;break;}
	}
}