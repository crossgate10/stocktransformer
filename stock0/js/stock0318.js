//取得瀏覽器視窗高度
function getBrowserHeight() {
    if ($.browser) {
        return document.compatMode == "CSS1Compat" ? document.documentElement.clientHeight :
                 document.body.clientHeight;
    } else {
        return self.innerHeight;
    }
}

//取得瀏覽器視窗寬度
function getBrowserWidth() {
    if ($.browser) {
        return document.compatMode == "CSS1Compat" ? document.documentElement.clientWidth :
                 document.body.clientWidth;
    } else {
        return self.innerWidth;
    }
} 

var size=[10,12,14,16,18,20];
function tdSizeS(){
	var BrowserWidth = getBrowserWidth(); //alert(BrowserWidth);
	if(BrowserWidth >= 1200){
		return size[0];
	}else if(BrowserWidth >= 980 && BrowserWidth <= 1199){
		return size[1];
	}else if(BrowserWidth >= 768 && BrowserWidth <= 979){
		return size[2];
	}else if(BrowserWidth >= 481 && BrowserWidth <= 767){
		return size[3];
	}else if(BrowserWidth >= 361 && BrowserWidth <= 480){
		return size[4];
	}else if(BrowserWidth <= 360){
		return size[5];
	}
}

function  tdSizeS4(n){
	switch(n)
	{
	case size[0]:{return n+2;break;}
	case size[1]:{return n+2;break;}
	case size[2]:{return n;break;}
	case size[3]:{return n;break;}
	case size[4]:{return n+2;break;}
	case size[5]:{return n+2;break;}
	}
}

function  tdSizeS6(n){
	switch(n)
	{
	case size[0]:{return n+6;break;}
	case size[1]:{return n+6;break;}
	case size[2]:{return n+6;break;}
	case size[3]:{return n+10;break;}
	case size[4]:{return n+10;break;}
	case size[5]:{return n+10;break;}
	}
}

function  tdSizeS8(n){
	switch(n)
	{
	case size[0]:{return n+10;break;}
	case size[1]:{return n+10;break;}
	case size[2]:{return n+10;break;}
	case size[3]:{return n+16;break;}
	case size[4]:{return n+18;break;}
	case size[5]:{return n+20;break;}
	}
}

$(function () {

	var colWidthS = tdSizeS();
	var colWidthS2 = colWidthS.toString()+"%";
	var colWidthS4 = tdSizeS4(colWidthS).toString()+"%";
	var colWidthS6 = tdSizeS6(colWidthS).toString()+"%";
	var colWidthS8 = tdSizeS8(colWidthS).toString()+"%";
	
	var rows = 50;
	function filterhandler(evt, ui) {
        var $toolbar = $grid.find('.pq-toolbar-search'),
            $value = $toolbar.find(".filterValue"),
            value = $value.val(),
            condition = $toolbar.find(".filterCondition").val(),
            dataIndx = $toolbar.find(".filterColumn").val(),
            filterObject = [];
		//判斷搜尋值(value)
		if (value != "") {
            filterObject.push({ dataIndx: dataIndx, condition: condition, value: value});
        } else {
			var CM = $grid.pqGrid("getColModel");
            for (var i = 0, len = CM.length; i < len; i++) {
                var dataIndx = CM[i].dataIndx;
                filterObject.push({ dataIndx: dataIndx, condition: condition, value: value });
            }
		}	
        $grid.pqGrid("filter", {
            oper: 'replace', data: filterObject
        });
    }
	
	function filterhandlerIndustry(evt, ui) {
        var $toolbar = $grid.find('.pq-toolbar-search'),
			industry = $toolbar.find(".filterIndustry").val(),
            filterObject = [];
		//判斷類別(industry)
		if((industry != ".0.") && (typeof(industry) != 'undefined')){
			filterObject.push({ dataIndx: 'industry', condition: "equal", value: industry});
		} else {
			var CM = $grid.pqGrid("getColModel");
            for (var i = 0, len = CM.length; i < len; i++) {
                var dataIndx = CM[i].dataIndx;
                filterObject.push({ dataIndx: dataIndx, condition: "equal", value: "" });
            }
		}	
        $grid.pqGrid("filter", {
            oper: 'replace', data: filterObject
        });
    }
	
	function filterRender(ui) {
            var val = ui.cellData,
                filter = ui.column.filter;
            if (filter && filter.on && filter.value) {
                var condition = filter.condition,
                    valUpper = val.toUpperCase(),
                    txt = filter.value,
                    txt = (txt == null) ? "" : txt.toString(),
                    txtUpper = txt.toUpperCase(),
                    indx = -1;
                if (condition == "end") {
                    indx = valUpper.lastIndexOf(txtUpper);
                    //if not at the end
                    if (indx + txtUpper.length != valUpper.length) {
                        indx = -1;
                    }
                } else if (condition == "contain") {
                    indx = valUpper.indexOf(txtUpper);
                } else if (condition == "begin") {
                    indx = valUpper.indexOf(txtUpper);
                    //if not at the beginning.
                    if (indx > 0) {
                        indx = -1;
                    }
                }
                if (indx >= 0) {
                    var txt1 = val.substring(0, indx);
                    var txt2 = val.substring(indx, indx + txt.length);
                    var txt3 = val.substring(indx + txt.length);
                    return txt1 + "<span style='background:yellow;color:#333;'>" + txt2 + "</span>" + txt3;
                } else {
                    return val;
                }
            } else {
                return val;
            }
        }
	
	var obj = {  
		width:'100%',
		height: '100%',
        numberCell:{show:false},
		freezeCols: 2,
		pageModel: { rPP: rows, type: "local"},
		toolbar: {
            cls: "pq-toolbar-search",
            items: [
                { type: 'select', style:"margin:0px 5px;", cls: "filterColumn",
                    listeners: [{ 'change': filterhandler}],
                    options: function (ui) {
                        var CM = ui.colModel;
						var obj = {};
						obj[CM[0].dataIndx] =CM[0].title;
						obj[CM[1].dataIndx] =CM[1].title;
						obj[CM[12].dataIndx] =CM[12].title;
						obj[CM[16].dataIndx] =CM[16].title;
						obj[CM[19].dataIndx] =CM[19].title;
						var opts = [];
                        opts.push(obj);
                        return opts;
                    }
                 },
                { type: 'select', cls: "filterCondition",
                    listeners: [{ 'change': filterhandler}],
                    options: [
                    { "begin": "開頭" },
                    { "contain": "包含" },
                    { "end": "結尾" }, 
					{ "equal": "等於" }, 
					{ "less": "小於" },
                    { "great": "大於" }
                    ]
                },
				{ type: 'textbox', attr: 'size=6 placeholder="查詢"', cls: "filterValue", listeners: [{ 'keyup': filterhandler}] },
				{ type: 'select', id: "select-choice-mini", cls: "filterIndustry",
                    listeners: [{ 'change': filterhandlerIndustry}],
                    options: [
                    { ".0.": "全部類別" },
                    { ".1.": "水泥工業" }, { ".2.": "食品工業" }, { ".3.": "塑膠工業" }, { ".4.": "紡織纖維" },
					{ ".5.": "電機機械" }, { ".6.": "電器電纜" }, { ".7.": "化學工業" }, { ".8.": "生技醫療" },
					{ ".9.": "玻璃陶瓷" }, { ".10.": "造紙工業" }, { ".11.": "鋼鐵工業" }, { ".12.": "橡膠工業" },
					{ ".13.": "汽車工業" }, { ".14.": "半導體業" }, { ".15.": "電腦週邊" }, { ".16.": "光電業" },
					{ ".17.": "通信網路" }, { ".18.": "零組件業" }, { ".19.": "電子通路" }, { ".20.": "資訊服務" },
					{ ".21.": "其他電子" }, { ".22.": "建材營造" }, { ".23.": "航運業" }, { ".24.": "觀光事業" },
					{ ".25.": "金融保險" }, { ".26.": "貿易百貨" }, { ".27.": "油電燃氣" }, { ".28.": "其他" },
					{ ".29.": "存託憑證" }, { ".30.": "受益證券" }, { ".31.": "指數基金" }, { ".32.": "全額交割" }
                    ]
                }
            ]
        },
        resizable: false,
		editable: false,
        showBottom: false,
		showTitle: false,
        collapsible: false,
		freezeBorders: false,
		columnBorders: false,
		rowBorders: false,
		selectionModel: { type: 'row' },
		dragColumns: { enabled: false }
    };
	
    obj.colModel = [
		{ title: "證卷代號", width:colWidthS4, dataIndx: "code", dataType: "string" ,sortable: false, render: filterRender},
        { title: "證卷名稱", width:colWidthS4, dataIndx: "company", dataType: "string" ,sortable: false, render: filterRender},
        { title: "漲跌", width:colWidthS2, dataIndx: "change", dataType: "float" ,sortable: false},
        { title: "收盤", width:colWidthS2, dataIndx: "close", dataType: "float" ,sortable: false},
	    { title: "次一日", align: "center", colModel: 
		[{ title: "漲停", width:colWidthS2, dataIndx: "nextRiseLimit", dataType: "float" ,sortable: false}, 
		{ title: "跌停", width:colWidthS2, dataIndx: "nextFallLimit", dataType: "float",sortable: false}]},
		{ title: "開盤", width:colWidthS2, dataIndx: "open", dataType: "float" ,sortable: false},
		{ title: "最高", width:colWidthS2, dataIndx: "dayHigh", dataType: "float" ,sortable: false},
		{ title: "最低", width:colWidthS2, dataIndx: "dayLow", dataType: "float" ,sortable: false},
		{ title: "平均每筆張數", width:colWidthS6, dataIndx: "volumePerTrade", dataType: "float" ,sortable: false},
		{ title: "成交數量(千股)", align: "center", colModel: 
		[{ title: "今日", width:colWidthS2, dataIndx: "volume", dataType: "float" ,sortable: false}, 
		{ title: "前一日", width:colWidthS2, dataIndx: "preVolume", dataType: "float",sortable: false}]},
		{ title: "5日RSI",  width:colWidthS4, dataIndx: "RsiPer5", dataType: "float" ,sortable: false, render: filterRender},
		{ title: "5日平均量", width:colWidthS6, dataIndx: "volumePer5", dataType: "float" ,sortable: false},
		{ title: "平均值", align: "center", colModel: 
		[{ title: "5日", width:colWidthS2, dataIndx: "pricePer5", dataType: "float" ,sortable: false}, 
		{ title: "20日",  width:colWidthS2, dataIndx: "pricePer20", dataType: "float",sortable: false}]},
		{ title: "10日乖離值",  width:colWidthS6, dataIndx: "BiasPer10", dataType: "float" ,sortable: false, render: filterRender},
		{ title: "去年來", align: "center", colModel: 
		[{ title: "最高", width:colWidthS2, dataIndx: "lastYearHigh", dataType: "float" ,sortable: false}, 
		{ title: "最低",  width:colWidthS2, dataIndx: "lastYearLow", dataType: "float",sortable: false}]},
		{ title: "交易所本益比",  width:colWidthS6, dataIndx: "PE", dataType: "float" ,sortable: false, render: filterRender},
		{ title: "期末股本(百萬元)", width:colWidthS8, dataIndx: "capital", dataType: "float" ,sortable: false},
		{ title: "類別", dataIndx: "industry", dataType: "string" ,sortable: false, hidden:true}
    ];
    obj.dataModel = { data: data };
	
	var $grid =$("#grid_json").pqGrid(obj);
	/**************************隱藏分頁欄位************************************/
	$("div").css('borderWidth', '0px');
	$("#grid_json").find(".pq-header-outer").css('borderWidth', '1px');
	$("#grid_json").find(".pq-grid-cont-outer").css('borderWidth', '1px');
	$("#grid_json").find(".pq-pager").hide();
	$("#grid_json").pqGrid( "refresh" );
	/**************************************************************************/

	
	function nextPage(){
		var pageN=parseInt($(".pq-pager-input").val())+1;
		$("#grid_json").pqGrid( "goToPage", { page: pageN} );
		$("#grid_json").pqGrid( "scrollRow", { rowIndxPage: 0 } );//最後一頁時的判斷?
	}
	function previous(){
		var pageP=parseInt($(".pq-pager-input").val())-1;
		if(pageP!=0){
			$("#grid_json").pqGrid( "goToPage", { page: pageP} );
			$("#grid_json").pqGrid( "scrollRow", { rowIndxPage: (rows-1) } ); 
		}
	}


	var preScrollValue=0;
	$('#grid_json').on('swipeend', function(e) { 
		var scrollValue=$(".pq-grid-cont-inner").scrollTop();
		if(scrollValue == 0){
			previous();	
		}else if(scrollValue==preScrollValue){
			nextPage();
			preScrollValue=0;
		}else{
			preScrollValue=scrollValue;
		} 
	});
	
	$('#grid_json').bind('mousewheel DOMMouseScroll', function(event){
		if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
			previous();	
		}else {
			nextPage();
		}
	});

	
	$(window).resize(function() {
		var colWidthS = tdSizeS();
		var colWidthS2 = colWidthS.toString()+"%";
		var colWidthS4 = tdSizeS4(colWidthS).toString()+"%";
		var colWidthS6 = tdSizeS6(colWidthS).toString()+"%";
		var colWidthS8 = tdSizeS8(colWidthS).toString()+"%";
		var colM=$( "#grid_json" ).pqGrid( "option", "colModel" );
		colM[0].width = colWidthS4;
        colM[1].width = colWidthS4;
        colM[2].width = colWidthS2;colM[3].width = colWidthS2;
		colM[4].colModel[0].width = colWidthS2;colM[4].colModel[1].width = colWidthS2;
		colM[5].width = colWidthS2;colM[6].width = colWidthS2;colM[7].width = colWidthS2;
		colM[8].width = colWidthS6;
		colM[9].colModel[0].width = colWidthS2;colM[9].colModel[1].width = colWidthS2;
		colM[10].width = colWidthS4;
		colM[11].width = colWidthS6;
		colM[12].colModel[0].width = colWidthS2;colM[12].colModel[1].width = colWidthS2;
		colM[13].width = colWidthS6;
		colM[14].colModel[0].width = colWidthS2;colM[14].colModel[1].width = colWidthS2;
		colM[15].width = colWidthS6;
		colM[16].width = colWidthS8;
		$("#grid_json").pqGrid( "option", "colModel", colM );	
	});
});
