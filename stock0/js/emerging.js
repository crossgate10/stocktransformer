$(function () {
	$( "#effect" ).hide();
	var colWidthS = tdSizeS();//找出寬度
	/*************************設定欄位寬度值************************/
	var colWidthS2 = colWidthS.toString()+"%"; 
	var colWidthS4 = tdSizeS4(colWidthS).toString()+"%";
	/*************************設定欄位寬度值************************/
	
	var $grid =$("#grid_json");//設定div位置
	var rows = 60;//每頁行數
	
	var obj = {  
		width:'100%',
		height: '100%',
        numberCell:{show:false},//關掉編號
		filterModel: { mode: 'OR' },
		freezeCols: 2,//固定左邊欄位
		pageModel: { rPP: rows, type: "local"}, //分頁
		toolbar: {
            cls: "pq-toolbar-search",
            items: [
				{ type: 'textbox', attr: 'size=6 placeholder="代號"', cls: "filterValue", listeners: [{ 'input': filterhandler}] },
				{ type: 'button', cls:"ui-btn ui-btn-inline",style: 'padding: 0; border: none; background: none; float: right;',
				listener: openMenu,
				label: '<a href=\"#menu\"><img class=\"myMenu\" src=\"./css/images/menu.png\"/></a>', 
				}
            ]
        },
        resizable: false,
		editable: false,
        showBottom: false,
		showTitle: false,
        collapsible: false,
		freezeBorders: false, 
		columnBorders: false, //框線
		rowBorders: false,
		selectionModel: { type: 'row' }, //tr class 
		dragColumns: { enabled: false }
    };
	
    obj.colModel = [
		{ title: "證卷代號", width:colWidthS4, dataIndx: "code", dataType: "string" ,sortable: false, render: filterRender},
        { title: "證卷名稱", width:colWidthS4, dataIndx: "company", dataType: "string" ,sortable: false, render: filterRender},
        { title: "前日均價", width:colWidthS4, dataIndx: "preAveragePrice", dataType: "float" ,sortable: false},
		{ title: "買價", width:colWidthS2, dataIndx: "buyingPrice", dataType: "float" ,sortable: false},
        { title: "賣價", width:colWidthS2, dataIndx: "sellingPrice", dataType: "float" ,sortable: false}	    
    ];
	
    obj.dataModel = { data: data };//放入資料
	$grid.pqGrid(obj);//產生table
	hidePage();//隱藏分頁欄位
	
	$('nav#menu').mmenu({//啟動 menu plugin
		extensions: ["pagedim-black","theme-white"],
		offCanvas: { position: "right" },
		'slidingSubmenus': false
	});	
	var menuAPI = $('nav#menu').data("mmenu");
	
	$("#stockDialog").dialog({  //上市摘要
	width: 'auto',
    height: 'auto',
		show: { effect: "slide"},
        autoOpen: false,   
        modal: true,  
        close: function() {   
            $("#stockDialog").dialog("close");  
        }   
	});  
	$("#stockAbstract").click(function() { //上市摘要
		$( "#stockDialog").dialog("open");   
	});
	
	$("#otcDialog").dialog({  //上櫃摘要
		width: 'auto',
		height: 'auto',
		show: { effect: "slide"},
        autoOpen: false,   
        modal: true,  
        close: function() {   
            $("#otcDialog").dialog("close");  
        }   
	});  
	$("#otcAbstract").click(function() { //上櫃摘要
		$( "#otcDialog").dialog("open");   
	});
	
	var preScrollValue=0;
	$grid.on('swipeup', function(e) { //偵測手指往上方向來決定是否換頁
		var scrollValue=$(".pq-grid-cont-inner").scrollTop();
		if(scrollValue==preScrollValue){ //滾輪確定到最底層
			nextPage();//下一頁
			preScrollValue=0;//回復預設
		}else{
			preScrollValue=scrollValue;//覆蓋
		} 
	});
	
	$grid.on('swipedown', function(e) { //偵測手指往下方向來決定是否換頁
		var scrollValue=$(".pq-grid-cont-inner").scrollTop();
		if(scrollValue == 0){ //滾輪最上層
			previous();	//上一頁
		}  
	});
	
	$grid.bind('mousewheel DOMMouseScroll', function(event){ //偵側滑鼠滾輪來決定是否換頁
		if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
			previous();	
		}else {
			nextPage();
		}
	});
	
	$(window).resize(function() {
		menuAPI.close();
		var colWidthS = tdSizeS();
		var colWidthS2 = colWidthS.toString()+"%";
		var colWidthS4 = tdSizeS4(colWidthS).toString()+"%";
		var colM=$grid.pqGrid( "option", "colModel" );
		colM[0].width = colWidthS4;colM[1].width = colWidthS4;
        colM[2].width = colWidthS4;colM[3].width = colWidthS2;
		colM[4].width = colWidthS2;
		$("#grid_json").pqGrid("option","colModel", colM );
		$("#grid_json").pqGrid("option", { width: getBrowserWidth(),height: getBrowserHeight() });  
		$("#grid_json").pqGrid('refresh');
	});
	
	function openMenu(){
		menuAPI.open();		
	}
	
	function filterhandler(evt, ui) {
        var $toolbar = $grid.find('.pq-toolbar-search'),
            $value = $toolbar.find(".filterValue"),
            value = $value.val(),
            condition = "begin",
            filterObject= [];
			filterObject.push({ dataIndx: "code", condition: condition, value: value });

        $grid.pqGrid("filter", {
            oper: 'replace', data: filterObject
        });
		$grid.pqGrid( "goToPage", { page: 1} ); //回到第一頁
		$grid.pqGrid( "scrollRow", { rowIndxPage: 0 } ); //滾輪到最上層
    }
	
	function filterRender(ui) {
        var val = ui.cellData,
                filter = ui.column.filter;
        if (filter && filter.on && filter.value) {
            var condition = filter.condition,
                valUpper = val.toUpperCase(),
                txt = filter.value,
                txt = (txt == null) ? "" : txt.toString(),
                txtUpper = txt.toUpperCase(),
                indx = -1;
            if (condition == "end") {
                indx = valUpper.lastIndexOf(txtUpper);
                if (indx + txtUpper.length != valUpper.length) {
                    indx = -1;
                }
            } else if (condition == "contain") {
                indx = valUpper.indexOf(txtUpper);
            } else if (condition == "begin") {
                indx = valUpper.indexOf(txtUpper);
                if (indx > 0) {
                    indx = -1;
                }
            }
            if (indx >= 0) {
                var txt1 = val.substring(0, indx);
                var txt2 = val.substring(indx, indx + txt.length);
                var txt3 = val.substring(indx + txt.length);
                return txt1 + "<span style='background:yellow;color:#333;'>" + txt2 + "</span>" + txt3;
            } else {
                return val;
            }
        } else {
            return val;
        }
    }

	function hidePage(){ //隱藏分頁
		$("div").css('borderWidth', '0px');
		$("#grid_json").find(".pq-header-outer").css('borderWidth', '1px');
		$("#grid_json").find(".pq-grid-cont-outer").css('borderWidth', '1px');
		$("#grid_json").find(".pq-pager").hide();
		$("#grid_json").pqGrid("refresh");
	}
	
	function nextPage(){ //下一頁
		var finalPage = parseInt($(".total")[0].textContent)+1;//最後一頁的判斷
		var pageN = parseInt($(".pq-pager-input").val())+1;
		$("#grid_json").pqGrid( "goToPage", { page: pageN} );
		if(pageN!=finalPage){//最後一頁的判斷
			$("#grid_json").pqGrid( "scrollRow", { rowIndxPage: 0 } );
		}
	}
	
	function previous(){ //上一頁
		var pageP=parseInt($(".pq-pager-input").val())-1;
		if(pageP!=0){ //第一頁的判斷
			$("#grid_json").pqGrid( "goToPage", { page: pageP} );
			$("#grid_json").pqGrid( "scrollRow", { rowIndxPage: (rows-1) } ); 
		}
	}
});

