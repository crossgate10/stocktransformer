/*!
 * jQuery UI Touch Punch 0.2.3.1
 *
 * Copyright 2011–2014, Dave Furfero
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * Depends:
 *  jquery.ui.widget.js
 *  jquery.ui.mouse.js
 */
!function(a){function b(a,b,c){if(!(a.originalEvent.touches.length>1)){a.preventDefault();var d=a.originalEvent.changedTouches[0],e=document.createEvent("MouseEvents");e.initMouseEvent(b,!0,!0,window,1,d.screenX,d.screenY,d.clientX,d.clientY,!1,!1,!1,!1,0,c),a.target.dispatchEvent(e)}}if(a.support.touch="ontouchend"in document,a.support.touch){var c,d=a.ui.mouse.prototype,e=d._mouseInit,f=!1,g=!1,h=d._mouseDestroy;d._touchStart=function(d){var e=this,h=e.widgetName;if(!c&&!e._touchHandled){var i=e.options.cancel,j=d.originalEvent.changedTouches[0],k=j.target,l="string"==typeof i&&k&&k.nodeName?a(k).closest(i).length:!1;if(!l&&("pqGrid"==h||e._mouseCapture(j))){if(e._touchHandled=c=!0,f){if(k&&k==f.target){var m=f.x-j.pageX,n=f.y-j.pageY,o=Math.sqrt(m*m+n*n);12>=o&&(g=!0,setTimeout(function(){g=!1},200))}}else f={x:j.pageX,y:j.pageY,target:k},setTimeout(function(){f=!1},400);e._touchMoved=!1,b(d,"mouseover"),b(d,"mousemove"),b(d,"mousedown")}}},d._touchMove=function(a){this._touchHandled&&(this._touchMoved=!0,b(a,"mousemove"))},d._touchEnd=function(a){this._touchHandled&&(b(a,"mouseup"),b(a,"mouseout"),this._touchMoved||b(a,"click"),this._touchMoved=!1,this._touchHandled=!1,c=!1,g&&b(a,"dblclick"))},d._mouseInit=function(){var b=this,c=this.eventNamespace,d={};d["touchstart"+c]=a.proxy(b,"_touchStart"),d["touchmove"+c]=a.proxy(b,"_touchMove"),d["touchend"+c]=a.proxy(b,"_touchEnd"),b.element.bind(d),e.call(b)},d._mouseDestroy=function(){var a=this,b=a.element,d=this.eventNamespace;this._touchHandled&&(c=!1,this._touchHandled=!1),b.unbind("touchstart"+d),b.unbind("touchmove"+d),b.unbind("touchend"+d),h.call(a)}}}(jQuery);